package ru.kozyrev.tm.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Getter
@XmlRootElement(name = "dataTransferObject")
public final class DataTransferObject implements Serializable {

    @XmlElementWrapper(name = "projects")
    @XmlElement(name = "project")
    @Nullable
    private List<Project> projects;

    @XmlElementWrapper(name = "tasks")
    @XmlElement(name = "task")
    @Nullable
    private List<Task> tasks;

    @XmlElementWrapper(name = "users")
    @XmlElement(name = "user")
    @Nullable
    private List<User> users;

    public final void load(@NotNull final ServiceLocator serviceLocator) throws Exception {
        @Nullable List<Project> projects = serviceLocator.getProjectService().findAll();
        if (projects == null) {
            projects = new ArrayList<>();
        }
        this.projects = new LinkedList<>(projects);

        @Nullable List<Task> tasks = serviceLocator.getTaskService().findAll();
        if (tasks == null) {
            tasks = new ArrayList<>();
        }
        this.tasks = new LinkedList<>(tasks);

        @Nullable List<User> users = serviceLocator.getUserService().findAll();
        if (users == null) {
            users = new ArrayList<>();
        }
        this.users = new LinkedList<>(users);
        for (int i = 0; i < users.size(); i++) {
            if ("admin".equals(users.get(i).getLogin())) {
                this.users.remove(i);
                break;
            }
        }
    }

    public final void unLoad(@NotNull final ServiceLocator serviceLocator) throws Exception {
        serviceLocator.getTaskService().removeAll();
        serviceLocator.getProjectService().removeAll();
        serviceLocator.getUserService().removeAll();

        serviceLocator.getUserService().persist(users);
        serviceLocator.getProjectService().persist(projects);
        serviceLocator.getTaskService().persist(tasks);
    }
}
