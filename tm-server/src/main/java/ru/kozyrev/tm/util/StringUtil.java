package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.exception.entity.IndexException;

public final class StringUtil {
    public final static int parseToInt(@NotNull final String num) throws IndexException {
        try {
            return Integer.parseInt(num);
        } catch (final NumberFormatException e) {
            throw new IndexException();
        }
    }
}
