package ru.kozyrev.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.entity.Session;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public final class AesUtil {
    @NotNull
    private static SecretKeySpec secretKey;

    public static final void setKey(String myKey) throws Exception {
        @NotNull final MessageDigest sha = MessageDigest.getInstance("SHA-1");
        byte[] key = myKey.getBytes(StandardCharsets.UTF_8);
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        secretKey = new SecretKeySpec(key, "AES");
    }

    private static final String encrypt(@NotNull final String strToEncrypt) throws Exception {
        setKey(ServerConst.SECRET_KEY);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        final byte[] data = cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(data);
    }

    private static final String decrypt(@NotNull final String strToDecrypt) throws Exception {
        setKey(ServerConst.SECRET_KEY);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        final byte[] data = Base64.getDecoder().decode(strToDecrypt);
        return new String(cipher.doFinal(data));
    }

    @Nullable
    public static final String encryptSession(@Nullable final Session session) {
        if (session == null) {
            return null;
        }
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(session);
            @NotNull final String sessionBASE64 = AesUtil.encrypt(json);
            return sessionBASE64;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static final Session decryptSession(@Nullable final String sessionBASE64) {
        if (sessionBASE64 == null || sessionBASE64.isEmpty()) {
            return null;
        }
        try {
            @NotNull final String json = AesUtil.decrypt(sessionBASE64);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Session session = objectMapper.readValue(json, Session.class);
            @NotNull final Session sessionSign = SignatureUtil.signSession(session);
            return sessionSign;
        } catch (Exception e) {
            return null;
        }
    }
}
