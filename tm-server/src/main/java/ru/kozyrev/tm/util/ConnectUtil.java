package ru.kozyrev.tm.util;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.mapper.*;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class ConnectUtil {
    public static final SqlSessionFactory getSqlSessionFactory() throws IOException {
        @NotNull final Properties properties = new Properties();
        @NotNull final InputStream fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "connection.properties");
        properties.load(fis);

        @NotNull final String driver = properties.getProperty("driver");
        @NotNull final String url = properties.getProperty("url");
        @NotNull final String username = properties.getProperty("username");
        @NotNull final String password = properties.getProperty("password");

        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("Server", transactionFactory, dataSource);

        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserMapper.class);
        configuration.addMapper(IProjectMapper.class);
        configuration.addMapper(ITaskMapper.class);
        configuration.addMapper(ISessionMapper.class);
        configuration.addMapper(IDbMapper.class);

        @NotNull final SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        return builder.build(configuration);
    }
}
