package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.enumerated.RoleType;

import java.io.Serializable;

@Getter
@Setter
public final class User extends AbstractEntity implements Serializable {
    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private RoleType roleType = null;
}
