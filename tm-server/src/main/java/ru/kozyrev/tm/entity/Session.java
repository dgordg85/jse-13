package ru.kozyrev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.enumerated.RoleType;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
public final class Session extends AbstractEntity implements Serializable {
    @Nullable
    private Long timestamp;

    @Nullable
    private String userId;

    @Nullable
    private RoleType roleType;

    @JsonIgnore
    @Nullable
    private String signature;

    @Override
    public final boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        @NotNull final Session session = (Session) o;
        return Objects.equals(getTimestamp(), session.getTimestamp()) &&
                Objects.equals(getUserId(), session.getUserId()) &&
                getRoleType() == session.getRoleType() &&
                Objects.equals(getSignature(), session.getSignature());
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getTimestamp(), getUserId(), getRoleType(), getSignature());
    }
}
