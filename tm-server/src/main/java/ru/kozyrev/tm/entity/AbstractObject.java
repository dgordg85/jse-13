package ru.kozyrev.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.enumerated.DocumentStatus;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public abstract class AbstractObject extends AbstractEntity implements Serializable {
    private final static long SerialVersionUID = 1L;

    @Nullable
    protected String userId;

    @Nullable
    protected Integer shortLink;

    @Nullable
    protected String name;

    @Nullable
    protected String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    protected Date dateStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    protected Date dateFinish;

    @Nullable
    protected DocumentStatus status = null;
}
