package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.exception.session.SessionNotValidException;

@NoArgsConstructor
public abstract class AbstractEndpoint {
    @NotNull
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected final Session checkSession(@Nullable final String sessionBASE64) throws Exception {
        @Nullable final Session session = serviceLocator.getSessionService().validateSession(sessionBASE64);
        if (session == null) {
            throw new SessionNotValidException();
        }
        return session;
    }
}
