package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IAdminEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.IAdminEndpoint")
@NoArgsConstructor
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {
    @NotNull
    public final static String URL = ServerConst.ADMIN_URL;

    public AdminEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void adminBinSave(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().binSave();
    }

    @Override
    public void adminBinLoad(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().binLoad(session);
    }

    @Override
    public void adminJsonLoadFasterXML(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonLoadFasterXML(session);
    }

    @Override
    public void adminJsonSaveFasterXML(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonSaveFasterXML();
    }

    @Override
    public void adminXmlLoadFasterXML(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlLoadFasterXML(session);
    }

    @Override
    public void adminXmlSaveFasterXML(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlSaveFasterXML();
    }

    @Override
    public void adminJsonSaveJaxB(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonSaveJaxB();
    }

    @Override
    public void adminJsonLoadJaxB(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().jsonLoadJaxB(session);
    }

    @Override
    public void adminXmlSaveJaxB(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlSaveJaxB();
    }

    @Override
    public void adminXmlLoadJaxB(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getAdminService().xmlLoadJaxB(session);
    }

    public void projectPersistList(
            @Nullable final String sessionBASE64,
            @Nullable final List<Project> projects
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getProjectService().persist(projects);
    }

    @Override
    public void projectRemoveAll(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getProjectService().removeAll();
    }

    @Nullable
    @Override
    public List<Project> projectFindAll(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    public void taskPersistList(
            @Nullable @WebParam(name = "sessionBASE64") final String sessionBASE64,
            @Nullable final List<Task> tasks
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getTaskService().persist(tasks);
    }

    @Nullable
    @Override
    public List<Task> taskFindAll(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        return serviceLocator.getTaskService().findAll();
    }

    @Override
    public void taskRemoveAll(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    public void userPersistList(
            @Nullable final String sessionBASE64,
            @Nullable final List<User> users
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getUserService().persist(users);
    }

    @Nullable
    @Override
    public List<User> userFindAll(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    public void userRemoveAll(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        checkForbidden(session);
        serviceLocator.getUserService().removeAll();
    }

    private final void checkForbidden(@Nullable final Session session) throws Exception {
        final boolean isSessionForbidden = serviceLocator.getSessionService().forbiddenSession(session);
        if (!isSessionForbidden) {
            throw new AccessForbiddenException();
        }
    }
}
