package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.ISessionEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.ISessionEndpoint")
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {
    @NotNull
    public final static String URL = ServerConst.SESSION_URL;

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public String openSession(
            @Nullable final String login,
            @Nullable final String hashPassword
    ) throws Exception {
        return serviceLocator.getSessionService().openSession(login, hashPassword);
    }

    @WebMethod
    public void closeSession(@Nullable final String sessionBASE64) throws Exception {
        serviceLocator.getSessionService().closeSession(sessionBASE64);
    }
}
