package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IUserEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;

import javax.jws.WebService;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.IUserEndpoint")
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {
    @NotNull
    public final static String URL = ServerConst.USER_URL;

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public User userFindOne(
            @Nullable final String sessionBASE64,
            @Nullable final String userId
    ) throws Exception {
        checkSession(sessionBASE64);
        return serviceLocator.getUserService().findOne(userId);
    }

    @Nullable
    @Override
    public User userGetYourself(
            @Nullable final String sessionBASE64
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getUserService().findOne(session.getUserId());
    }

    @Nullable
    @Override
    public User userPersist(@Nullable final User user) throws Exception {
        return serviceLocator.getUserService().persistSaltPass(user);
    }

    @Nullable
    @Override
    public User userMerge(
            @Nullable final String sessionBASE64,
            @Nullable final User user
    ) throws Exception {
        checkSession(sessionBASE64);
        return serviceLocator.getUserService().mergeSaltPass(user);
    }

    @Override
    public void userRemove(
            @Nullable final String sessionBASE64,
            @Nullable final String userId
    ) throws Exception {
        checkSession(sessionBASE64);
        serviceLocator.getUserService().remove(userId);
    }

    @Override
    public void userRemoveBySession(
            @Nullable final String sessionBASE64
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getUserService().remove(session.getUserId());
    }

    @Nullable
    @Override
    public User userGetUserByLogin(
            @Nullable final String sessionBASE64,
            @Nullable final String login
    ) throws Exception {
        checkSession(sessionBASE64);
        return serviceLocator.getUserService().getUserByLogin(login);
    }

    @Override
    public void userUpdatePassword(
            @Nullable final String sessionBASE64,
            @Nullable final String currentPassword,
            @Nullable final String newPassword
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getUserService().updateUserPassword(currentPassword, newPassword, session.getUserId());
    }

    @NotNull
    public RoleType getUserRole(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getUserService().getUserRole(session.getUserId());
    }
}
