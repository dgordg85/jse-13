package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.ITaskEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.ITaskEndpoint")
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {
    @NotNull
    public final static String URL = ServerConst.TASK_URL;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public Task taskFindOneAuth(
            @Nullable final String sessionBASE64,
            @Nullable final String id
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getTaskService().findOne(id, session.getUserId());
    }

    @Nullable
    @Override
    public List<Task> taskFindAllAuth(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    public void taskRemoveAuth(
            @Nullable final String sessionBASE64,
            @Nullable final String taskId
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getTaskService().remove(taskId, session.getUserId());
    }

    @Override
    public void taskRemoveAllAuth(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getTaskService().removeAll(session.getUserId());
    }

    @Nullable
    @Override
    public String taskGetIdByShortLink(
            @Nullable final String sessionBASE64,
            @Nullable final String num
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getTaskService().getEntityIdByShortLink(num, session.getUserId());
    }

    @Nullable
    @Override
    public List<Task> taskFindWord(
            @Nullable final String sessionBASE64,
            @NotNull final String word
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getTaskService().findWord(word, session.getUserId());
    }

    @Nullable
    @Override
    public Task taskPersistAuth(
            @Nullable final String sessionBASE64,
            @Nullable final Task task
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getTaskService().persist(task, session.getUserId());
    }

    @Nullable
    @Override
    public Task taskMergeAuth(
            @Nullable final String sessionBASE64,
            @Nullable final Task task
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getTaskService().merge(task, session.getUserId());
    }

    @Override
    public void taskRemoveAllByProjectAuth(
            @Nullable final String sessionBASE64,
            @Nullable final String projectId
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getTaskService().removeAll(projectId, session.getUserId());
    }

    @Override
    public void taskRemoveAllByProjectNumAuth(
            @Nullable final String sessionBASE64,
            @Nullable final String projectNum
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getTaskService().removeAllByProjectNum(projectNum, session.getUserId());
    }

    @Nullable
    @Override
    public List<Task> taskFindAllByProjectAuth(
            @Nullable final String sessionBASE64,
            @Nullable final String projectId
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getTaskService().findAll(projectId, session.getUserId());
    }

    @Nullable
    @Override
    public List<Task> taskFindAllByProjectAuthSort(
            @Nullable final String sessionBASE64,
            @Nullable final String projectId,
            @NotNull final Column column,
            @NotNull final Direction direction
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getTaskService().findAll(projectId, column, direction, session.getUserId());
    }

    @Override
    public void taskRemoveByShortLink(
            @Nullable final String sessionBASE64,
            @Nullable final String num
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getTaskService().removeEntityByShortLink(num, session.getUserId());
    }
}
