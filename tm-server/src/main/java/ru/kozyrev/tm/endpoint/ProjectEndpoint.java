package ru.kozyrev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.IProjectEndpoint;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kozyrev.tm.api.endpoint.IProjectEndpoint")
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {
    @NotNull
    public final static String URL = ServerConst.PROJECT_URL;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public Project projectFindOneAuth(
            @Nullable final String sessionBASE64,
            @Nullable final String id
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getProjectService().findOne(id, session.getUserId());
    }

    @Nullable
    @Override
    public List<Project> projectFindAllAuth(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    public void projectRemoveAuth(
            @Nullable final String sessionBASE64,
            @Nullable final String projectId
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getProjectService().remove(projectId, session.getUserId());
    }

    @Override
    public void projectRemoveAllAuth(@Nullable final String sessionBASE64) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }

    @Nullable
    @Override
    public String projectGetIdByShortLink(
            @Nullable final String sessionBASE64,
            @Nullable final String num
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getProjectService().getEntityIdByShortLink(num, session.getUserId());
    }

    @Nullable
    @Override
    public List<Project> projectFindWord(
            @Nullable final String sessionBASE64,
            @NotNull final String word
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getProjectService().findWord(word, session.getUserId());
    }

    @Nullable
    @Override
    public Project projectPersistAuth(
            @Nullable final String sessionBASE64,
            @Nullable final Project project
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getProjectService().persist(project, session.getUserId());
    }

    @Nullable
    @Override
    public Project projectMergeAuth(
            @Nullable final String sessionBASE64,
            @Nullable final Project project
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getProjectService().merge(project, session.getUserId());
    }

    @Nullable
    @Override
    public List<Project> projectFindAllBySorting(
            @Nullable final String sessionBASE64,
            @NotNull final Column column,
            @NotNull final Direction direction
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        return serviceLocator.getProjectService().findAll(column, direction, session.getUserId());
    }

    @Override
    public void projectRemoveByShortLink(
            @Nullable final String sessionBASE64,
            @Nullable final String num
    ) throws Exception {
        @NotNull final Session session = checkSession(sessionBASE64);
        serviceLocator.getProjectService().removeEntityByShortLink(num, session.getUserId());
    }
}
