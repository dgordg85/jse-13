package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @WebMethod
    @Nullable
    Project projectFindOneAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "id") String id
    ) throws Exception;

    @WebMethod
    @Nullable
    List<Project> projectFindAllAuth(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void projectRemoveAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "projectId") String projectId
    ) throws Exception;

    @WebMethod
    void projectRemoveAllAuth(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    @Nullable
    String projectGetIdByShortLink(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "num") String num
    ) throws Exception;

    @WebMethod
    @Nullable
    List<Project> projectFindWord(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @NotNull @WebParam(name = "word") String word
    ) throws Exception;

    @WebMethod
    @Nullable
    Project projectPersistAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable Project project
    ) throws Exception;

    @WebMethod
    @Nullable
    Project projectMergeAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable Project project
    ) throws Exception;

    @WebMethod
    @Nullable
    List<Project> projectFindAllBySorting(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @NotNull Column column,
            @NotNull Direction direction
    ) throws Exception;

    @WebMethod
    void projectRemoveByShortLink(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "num") String num
    ) throws Exception;
}
