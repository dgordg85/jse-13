package ru.kozyrev.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.ISessionMapper;
import ru.kozyrev.tm.entity.Session;

public interface ISessionService extends IEntityService<Session> {
    @Nullable
    Session persist(@Nullable Session session) throws Exception;

    @Nullable
    String openSession(@Nullable String login, @Nullable String hashPassword) throws Exception;

    Session validateSession(@Nullable String session) throws Exception;

    void closeSession(@Nullable String sessionBASE64) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);

    @Nullable
    Session findOne(@Nullable String id) throws Exception;

    @NotNull
    ISessionMapper getMapper(@NotNull SqlSession session) throws Exception;

    boolean forbiddenSession(Session session) throws Exception;
}
