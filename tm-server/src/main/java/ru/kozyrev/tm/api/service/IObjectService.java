package ru.kozyrev.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.IObjectMapper;

import java.util.List;

public interface IObjectService<T> extends IService<T> {
    @Nullable
    T persist(@Nullable T object) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<T> object) throws Exception;

    @Nullable
    T findOne(@Nullable String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<T> findAll(@Nullable String userId) throws Exception;

    @Nullable
    T persist(@Nullable T object, @Nullable String userId) throws Exception;

    @Nullable
    T merge(@Nullable T object, @Nullable String userId) throws Exception;

    void remove(@Nullable String objectId, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    List<T> findWord(@Nullable String word, @Nullable String userId) throws Exception;

    @Nullable
    String getEntityIdByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    void removeEntityByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    @NotNull
    IObjectMapper<T> getMapper(@NotNull SqlSession session) throws Exception;
}
