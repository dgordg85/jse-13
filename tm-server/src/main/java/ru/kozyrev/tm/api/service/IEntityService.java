package ru.kozyrev.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.IEntityMapper;

import java.util.List;

public interface IEntityService<T> extends IService<T> {
    @Nullable
    T findOne(@Nullable String id) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    @Nullable
    T persist(@Nullable T entity) throws Exception;

    @Nullable
    T merge(@Nullable T entity) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<T> entity) throws Exception;

    IEntityMapper<T> getMapper(@NotNull SqlSession session) throws Exception;
}
