package ru.kozyrev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.entity.EntityConst;
import ru.kozyrev.tm.constant.sql.TaskSQL;
import ru.kozyrev.tm.entity.Task;

import java.util.List;

public interface ITaskMapper extends IObjectMapper<Task> {
    @Select(TaskSQL.PERSIST)
    void persist(@NotNull Task task) throws Exception;

    @Update(TaskSQL.MERGE)
    void merge(@NotNull Task task) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ONE)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    Task findOne(
            @NotNull @Param(EntityConst.ID) String id,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ONE_BY_SHORT_LINK)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    Task findOneByShortLink(
            @NotNull @Param(EntityConst.SHORT_LINK) Integer shortLink,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAll() throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_USER_ID)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByUserId(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Delete(TaskSQL.REMOVE)
    void remove(
            @NotNull @Param(EntityConst.ID) String id,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Select(TaskSQL.REMOVE_ALL)
    void removeAll() throws Exception;

    @Delete(TaskSQL.REMOVE_ALL_BY_USER_ID)
    void removeAllByUserId(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Delete(TaskSQL.REMOVE_ALL_BY_PROJECT)
    void removeAllByProject(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_PROJECT)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByProjectId(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_NUM)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByNum(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_START)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByDateStart(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_FINISH)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByDateFinish(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_STATUS)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByStatus(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_NUM_DESC)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByNumDesc(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_START_DESC)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByDateStartDesc(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_FINISH_DESC)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByDateFinishDesc(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_ALL_BY_STATUS_DESC)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findAllByStatusDesc(
            @NotNull @Param(EntityConst.PROJECT_ID) String projectId,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(TaskSQL.FIND_WORD)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.TASK_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.PROJECT_ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Task> findWord(
            @NotNull @Param(EntityConst.WORD) String word,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Update(TaskSQL.RESET_INCREMENT)
    void resetIncrement();
}
