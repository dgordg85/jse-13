package ru.kozyrev.tm.api.mapper;

import org.apache.ibatis.annotations.Update;
import ru.kozyrev.tm.constant.sql.DbSQL;

public interface IDbMapper {
    @Update(DbSQL.DROP_DB_TASK_MANAGER)
    void dropDB();

    @Update(DbSQL.CREATE_DB_TASK_MANAGER)
    void createDB();

    @Update(DbSQL.CREATE_USERS_TABLE)
    void createUserTable();

    @Update(DbSQL.CREATE_SESSIONS_TABLE)
    void createSessionTable();

    @Update(DbSQL.CREATE_PROJECTS_TABLE)
    void createProjectTable();

    @Update(DbSQL.CREATE_TASKS_TABLE)
    void createTaskTable();
}
