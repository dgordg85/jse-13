package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {
    @WebMethod
    String openSession(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "hashPassword") String hashPassword
    ) throws Exception;

    @WebMethod
    void closeSession(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;
}
