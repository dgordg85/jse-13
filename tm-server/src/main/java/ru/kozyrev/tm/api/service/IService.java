package ru.kozyrev.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.IMapper;

import java.util.List;

public interface IService<T> {
    @Nullable
    T persist(@Nullable T entity) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<T> entity) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);

    IMapper<T> getMapper(@NotNull SqlSession session) throws Exception;
}
