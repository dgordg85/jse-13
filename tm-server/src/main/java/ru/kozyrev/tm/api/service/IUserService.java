package ru.kozyrev.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.IUserMapper;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;

import java.util.List;

public interface IUserService extends IEntityService<User> {
    @Nullable
    List<User> findAll() throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User findOne(@Nullable String id) throws Exception;

    @Nullable
    User persist(@Nullable User user) throws Exception;

    @Nullable
    User persistSaltPass(@Nullable User user) throws Exception;

    @Nullable
    User merge(@Nullable User user) throws Exception;

    @Nullable
    User mergeSaltPass(@Nullable User user) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void persist(@Nullable List<User> users) throws Exception;

    @Nullable
    User getUserByLogin(@Nullable String login) throws Exception;

    void updateUserPassword(@Nullable String passC, @Nullable String hashPass, @Nullable String userId) throws Exception;

    @NotNull
    RoleType getUserRole(@Nullable String userId) throws Exception;

    @NotNull
    IUserMapper getMapper(@NotNull SqlSession session) throws Exception;
}
