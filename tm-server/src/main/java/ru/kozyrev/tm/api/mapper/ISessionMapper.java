package ru.kozyrev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.entity.EntityConst;
import ru.kozyrev.tm.constant.sql.SessionSQL;
import ru.kozyrev.tm.entity.Session;

import java.util.List;

public interface ISessionMapper extends IEntityMapper<Session> {

    @Update(SessionSQL.PERSIST)
    void persist(@NotNull Session session) throws Exception;

    @Update(SessionSQL.MERGE)
    void merge(@NotNull Session session) throws Exception;

    @Nullable
    @Select(SessionSQL.FIND_ONE)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.SESSION_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.ROLE_TYPE, column = FieldConst.ROLE_TYPE),
            @Result(property = EntityConst.SIGNATURE, column = FieldConst.SIGNATURE),
            @Result(property = EntityConst.TIMESTAMP, column = FieldConst.TIMESTAMP),
    })
    Session findOne(@NotNull @Param(EntityConst.ID) String id) throws Exception;

    @Nullable
    @Select(SessionSQL.FIND_ONE_BY_USER_ID)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.SESSION_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.ROLE_TYPE, column = FieldConst.ROLE_TYPE),
            @Result(property = EntityConst.SIGNATURE, column = FieldConst.SIGNATURE),
            @Result(property = EntityConst.TIMESTAMP, column = FieldConst.TIMESTAMP),
    })
    Session findOneByUserId(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(SessionSQL.FIND_ALL)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.SESSION_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.ROLE_TYPE, column = FieldConst.ROLE_TYPE),
            @Result(property = EntityConst.SIGNATURE, column = FieldConst.SIGNATURE),
            @Result(property = EntityConst.TIMESTAMP, column = FieldConst.TIMESTAMP),
    })
    List<Session> findAll() throws Exception;

    @Delete(SessionSQL.REMOVE)
    void remove(@NotNull @Param(EntityConst.ID) final String id) throws Exception;

    @Delete(SessionSQL.REMOVE_ALL)
    void removeAll() throws Exception;
}
