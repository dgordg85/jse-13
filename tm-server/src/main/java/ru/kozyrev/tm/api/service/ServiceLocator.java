package ru.kozyrev.tm.api.service;

import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {
    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAdminService getAdminService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();
}
