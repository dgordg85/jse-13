package ru.kozyrev.tm.api.mapper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IEntityMapper<T> extends IMapper<T> {
    void persist(@NotNull T entity) throws Exception;

    void merge(@NotNull T entity) throws Exception;

    @Nullable
    T findOne(@NotNull String id) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    void remove(@NotNull String id) throws Exception;

    void removeAll() throws Exception;
}
