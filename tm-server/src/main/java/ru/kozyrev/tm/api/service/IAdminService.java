package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Session;

public interface IAdminService {
    void binSave() throws Exception;

    void binLoad(@Nullable Session session) throws Exception;

    void jsonLoadFasterXML(@Nullable Session session) throws Exception;

    void jsonSaveFasterXML() throws Exception;

    void jsonSaveJaxB() throws Exception;

    void jsonLoadJaxB(@Nullable Session session) throws Exception;

    void xmlLoadFasterXML(@Nullable Session session) throws Exception;

    void xmlSaveFasterXML() throws Exception;

    void xmlSaveJaxB() throws Exception;

    void xmlLoadJaxB(@Nullable Session session) throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);

    @NotNull String getDocumentStatusStr(@Nullable String value) throws Exception;

    @NotNull String getRoleTypeStr(@Nullable String value) throws Exception;
}
