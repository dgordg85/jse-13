package ru.kozyrev.tm.api.mapper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IObjectMapper<T> extends IMapper<T> {
    void persist(@NotNull T object) throws Exception;

    void merge(@NotNull T object) throws Exception;

    @Nullable
    T findOne(@NotNull String id, @NotNull String userId) throws Exception;

    @Nullable
    T findOneByShortLink(@NotNull Integer shortLink, @NotNull String userId) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    @Nullable
    List<T> findAllByUserId(@NotNull String userId) throws Exception;

    void remove(@NotNull String id, @NotNull String userId) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@NotNull String userId) throws Exception;

    @Nullable
    List<T> findWord(@NotNull String word, @NotNull String userId) throws Exception;

    void resetIncrement();
}
