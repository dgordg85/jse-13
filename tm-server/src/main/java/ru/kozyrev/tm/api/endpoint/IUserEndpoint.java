package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint {
    @Nullable
    User userFindOne(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "userId") String userId
    ) throws Exception;

    @WebMethod
    @Nullable
    User userGetYourself(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64
    ) throws Exception;

    @WebMethod
    @Nullable
    User userPersist(@Nullable User user) throws Exception;

    @WebMethod
    @Nullable
    User userMerge(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable User user
    ) throws Exception;

    @WebMethod
    void userRemove(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "userId") String userId
    ) throws Exception;

    @WebMethod
    void userRemoveBySession(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64
    ) throws Exception;

    @WebMethod
    @Nullable
    User userGetUserByLogin(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "login") String login
    ) throws Exception;

    @WebMethod
    void userUpdatePassword(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "currentPassword") String currentPassword,
            @Nullable @WebParam(name = "newPassword") String newPassword
    ) throws Exception;

    @WebMethod
    @NotNull RoleType getUserRole(@Nullable String sessionBASE64) throws Exception;
}
