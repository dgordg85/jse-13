package ru.kozyrev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    @Nullable
    Task taskFindOneAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "id") String id
    ) throws Exception;

    @WebMethod
    @Nullable
    List<Task> taskFindAllAuth(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    void taskRemoveAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "taskId") String taskId
    ) throws Exception;

    @WebMethod
    void taskRemoveAllAuth(@Nullable @WebParam(name = "sessionBASE64") String sessionBASE64) throws Exception;

    @WebMethod
    @Nullable
    String taskGetIdByShortLink(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "num") String num
    ) throws Exception;

    @WebMethod
    @Nullable
    List<Task> taskFindWord(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @NotNull @WebParam(name = "word") String word
    ) throws Exception;

    @WebMethod
    @Nullable
    Task taskPersistAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable Task task
    ) throws Exception;

    @WebMethod
    @Nullable
    Task taskMergeAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable Task task
    ) throws Exception;

    @WebMethod
    void taskRemoveAllByProjectAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "projectId") String projectId
    ) throws Exception;

    @WebMethod
    void taskRemoveAllByProjectNumAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "projectNum") String projectNum
    ) throws Exception;

    @WebMethod
    @Nullable
    List<Task> taskFindAllByProjectAuth(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "projectId") String projectId
    ) throws Exception;

    @WebMethod
    @Nullable
    List<Task> taskFindAllByProjectAuthSort(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "projectId") String projectId,
            @NotNull Column column,
            @NotNull Direction direction
    ) throws Exception;

    @WebMethod
    void taskRemoveByShortLink(
            @Nullable @WebParam(name = "sessionBASE64") String sessionBASE64,
            @Nullable @WebParam(name = "num") String num
    ) throws Exception;
}
