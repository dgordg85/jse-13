package ru.kozyrev.tm.api.mapper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IMapper<T> {
    void persist(@NotNull final T entity) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    void removeAll() throws Exception;
}
