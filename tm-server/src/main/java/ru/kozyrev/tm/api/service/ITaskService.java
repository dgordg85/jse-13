package ru.kozyrev.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.ITaskMapper;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.List;

public interface ITaskService extends IObjectService<Task> {
    @Nullable
    List<Task> findAll() throws Exception;

    void removeAll() throws Exception;

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);

    @Nullable
    Task findOne(@Nullable String id, @Nullable final String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String userId) throws Exception;

    void remove(@Nullable String id, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    List<Task> findWord(@Nullable String word, @Nullable String userId) throws Exception;

    @Nullable
    String getEntityIdByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    void removeEntityByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String projectId, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String projectId, @Nullable String userId) throws Exception;

    void removeAllByProjectNum(@Nullable String projectNum, @Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String projectId, @NotNull Column column, @NotNull Direction direction, @Nullable String userId) throws Exception;

    @NotNull
    ITaskMapper getMapper(@NotNull SqlSession session) throws Exception;
}
