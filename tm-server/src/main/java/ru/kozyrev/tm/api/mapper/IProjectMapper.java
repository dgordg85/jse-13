package ru.kozyrev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.entity.EntityConst;
import ru.kozyrev.tm.constant.sql.ProjectSQL;
import ru.kozyrev.tm.entity.Project;

import java.util.List;

public interface IProjectMapper extends IObjectMapper<Project> {
    @Update(ProjectSQL.PERSIST)
    void persist(@NotNull Project project) throws Exception;

    @Update(ProjectSQL.MERGE)
    void merge(@NotNull Project project) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ONE)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    Project findOne(
            @NotNull @Param(EntityConst.ID) String id,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ONE_BY_SHORT_LINK)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    Project findOneByShortLink(
            @NotNull @Param(EntityConst.SHORT_LINK) Integer shortLink,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAll() throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_USER_ID)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByUserId(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_NUM)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByNum(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_START)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByDateStart(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_FINISH)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByDateFinish(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_STATUS)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByStatus(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_NUM_DESC)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByNumDesc(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_START_DESC)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByDateStartDesc(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_FINISH_DESC)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByDateFinishDesc(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_ALL_BY_STATUS_DESC)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findAllByStatusDesc(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Delete(ProjectSQL.REMOVE)
    void remove(
            @NotNull @Param(EntityConst.ID) String id,
            @NotNull @Param(EntityConst.USER_ID) String userId
    ) throws Exception;

    @Delete(ProjectSQL.REMOVE_ALL)
    void removeAll() throws Exception;

    @Delete(ProjectSQL.REMOVE_ALL_BY_USER_ID)
    void removeAllByUserId(@NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Nullable
    @Select(ProjectSQL.FIND_WORD)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.PROJECT_ID),
            @Result(property = EntityConst.USER_ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.SHORT_LINK, column = FieldConst.SHORT_LINK),
            @Result(property = EntityConst.NAME, column = FieldConst.NAME),
            @Result(property = EntityConst.DESCRIPTION, column = FieldConst.DESCRIPTION),
            @Result(property = EntityConst.DATE_START, column = FieldConst.DATE_START),
            @Result(property = EntityConst.DATE_FINISH, column = FieldConst.DATE_FINISH),
            @Result(property = EntityConst.STATUS, column = FieldConst.STATUS)
    })
    List<Project> findWord(
            @NotNull @Param(EntityConst.WORD) String word,
            @NotNull @Param(EntityConst.USER_ID) String userId) throws Exception;

    @Update(ProjectSQL.RESET_INCREMENT)
    void resetIncrement();
}
