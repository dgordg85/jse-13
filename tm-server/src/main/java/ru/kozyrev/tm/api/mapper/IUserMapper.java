package ru.kozyrev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.entity.EntityConst;
import ru.kozyrev.tm.constant.sql.UserSQL;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserMapper extends IEntityMapper<User> {
    @Select(UserSQL.PERSIST)
    void persist(@NotNull User user) throws Exception;

    @Update(UserSQL.MERGE)
    void merge(@NotNull User user) throws Exception;

    @Nullable
    @Select(UserSQL.FIND_ONE)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.LOGIN, column = FieldConst.LOGIN),
            @Result(property = EntityConst.PASSWORD_HASH, column = FieldConst.PASSWORD_HASH),
            @Result(property = EntityConst.ROLE_TYPE, column = FieldConst.ROLE_TYPE),
    })
    User findOne(@NotNull @Param(EntityConst.ID) String id) throws Exception;

    @Nullable
    @Select(UserSQL.FIND_ONE_BY_LOGIN)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.LOGIN, column = FieldConst.LOGIN),
            @Result(property = EntityConst.PASSWORD_HASH, column = FieldConst.PASSWORD_HASH),
            @Result(property = EntityConst.ROLE_TYPE, column = FieldConst.ROLE_TYPE),
    })
    User findOneByLogin(@NotNull @Param(EntityConst.LOGIN) String login) throws Exception;

    @Nullable
    @Select(UserSQL.FIND_ALL)
    @Results(value = {
            @Result(property = EntityConst.ID, column = FieldConst.USER_ID),
            @Result(property = EntityConst.LOGIN, column = FieldConst.LOGIN),
            @Result(property = EntityConst.PASSWORD_HASH, column = FieldConst.PASSWORD_HASH),
            @Result(property = EntityConst.ROLE_TYPE, column = FieldConst.ROLE_TYPE),
    })
    List<User> findAll() throws Exception;

    @Delete(UserSQL.REMOVE)
    void remove(@NotNull String id) throws Exception;

    @Delete(UserSQL.REMOVE_ALL)
    void removeAll() throws Exception;
}