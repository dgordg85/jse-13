package ru.kozyrev.tm.exception.entity;

public final class EmptyEntityException extends Exception {
    public EmptyEntityException() {
        super("Empty entity!");
    }
}
