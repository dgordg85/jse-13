package ru.kozyrev.tm.exception.enumerated;

public final class UnknownTypeException extends Exception {
    public UnknownTypeException() {
        super("Unknown sort column!");
    }
}
