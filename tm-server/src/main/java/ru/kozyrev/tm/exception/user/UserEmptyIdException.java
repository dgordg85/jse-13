package ru.kozyrev.tm.exception.user;

public final class UserEmptyIdException extends Exception {
    public UserEmptyIdException() {
        super("No user id");
    }
}
