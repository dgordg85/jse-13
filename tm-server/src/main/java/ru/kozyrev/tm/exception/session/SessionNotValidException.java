package ru.kozyrev.tm.exception.session;

public final class SessionNotValidException extends Exception {
    public SessionNotValidException() {
        super("Session not valid!");
    }
}
