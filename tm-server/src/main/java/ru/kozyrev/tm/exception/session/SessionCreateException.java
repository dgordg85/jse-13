package ru.kozyrev.tm.exception.session;

public final class SessionCreateException extends Exception {
    public SessionCreateException() {
        super("Session can't bet created!");
    }
}
