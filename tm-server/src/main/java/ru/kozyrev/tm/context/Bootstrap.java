package ru.kozyrev.tm.context;

import lombok.Getter;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.*;
import ru.kozyrev.tm.api.mapper.IDbMapper;
import ru.kozyrev.tm.api.service.*;
import ru.kozyrev.tm.endpoint.*;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.service.*;
import ru.kozyrev.tm.util.ConnectUtil;
import ru.kozyrev.tm.util.HashUtil;

import javax.xml.ws.Endpoint;


@Getter
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IAdminService adminService;

    @NotNull
    private IProjectEndpoint projectEndpoint;

    @NotNull
    private ITaskEndpoint taskEndpoint;

    @NotNull
    private IUserEndpoint userEndpoint;

    @NotNull
    private IAdminEndpoint adminEndpoint;

    @NotNull
    private ISessionEndpoint sessionEndpoint;

    @NotNull
    private SqlSessionFactory sqlSessionFactory;

    public void start() throws Exception {
        sqlSessionFactory = ConnectUtil.getSqlSessionFactory();
        createDB();
        initServices();
        initEndpoint();
        createDefaultUsers();
        publicEndpoints();
    }

    public final void createDefaultUsers() throws Exception {
        if (userService.getUserByLogin("user") == null) {
            @NotNull final User user = new User();
            user.setLogin("user");
            user.setPasswordHash(HashUtil.getHash("user"));
            user.setRoleType(RoleType.USER);
            userService.persistSaltPass(user);
        }

        if (userService.getUserByLogin("admin") == null) {
            @NotNull final User admin = new User();
            admin.setLogin("admin");
            admin.setPasswordHash(HashUtil.getHash("admin"));
            admin.setRoleType(RoleType.ADMIN);
            userService.persistSaltPass(admin);
        }
    }

    private final void initEndpoint() {
        projectEndpoint = new ProjectEndpoint(this);
        taskEndpoint = new TaskEndpoint(this);
        userEndpoint = new UserEndpoint(this);
        adminEndpoint = new AdminEndpoint(this);
        sessionEndpoint = new SessionEndpoint(this);
    }

    public final void initServices() {
        projectService = new ProjectService();
        projectService.setServiceLocator(this);

        taskService = new TaskService();
        taskService.setServiceLocator(this);

        userService = new UserService();
        userService.setServiceLocator(this);

        sessionService = new SessionService();
        sessionService.setServiceLocator(this);

        adminService = new AdminService();
        adminService.setServiceLocator(this);
    }

    private final void publicEndpoints() {
        publicAddress(ProjectEndpoint.URL, (ProjectEndpoint) projectEndpoint);
        publicAddress(TaskEndpoint.URL, (TaskEndpoint) taskEndpoint);
        publicAddress(UserEndpoint.URL, (UserEndpoint) userEndpoint);
        publicAddress(AdminEndpoint.URL, (AdminEndpoint) adminEndpoint);
        publicAddress(SessionEndpoint.URL, (SessionEndpoint) sessionEndpoint);
    }

    private final void publicAddress(@NotNull final String url, @NotNull final AbstractEndpoint endpoint) {
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public final void createDB() {
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        try {
            session.getMapper(IDbMapper.class).dropDB();
            session.getMapper(IDbMapper.class).createDB();
            session.getMapper(IDbMapper.class).createUserTable();
            session.getMapper(IDbMapper.class).createSessionTable();
            session.getMapper(IDbMapper.class).createProjectTable();
            session.getMapper(IDbMapper.class).createTaskTable();
            session.commit();
        } catch (final Exception e) {
            session.rollback();
        } finally {
            session.close();
        }
    }
}
