package ru.kozyrev.tm.constant.sql;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.db.TableConst;

public final class DbSQL {
    @NotNull
    public static final String DROP_DB_TASK_MANAGER = "DROP DATABASE IF EXISTS " + TableConst.DB_NAME + ";";

    @NotNull
    public static final String CREATE_DB_TASK_MANAGER = "CREATE DATABASE IF NOT EXISTS " + TableConst.DB_NAME + "\n" +
            "COLLATE='utf8_general_ci'\n" +
            ";";

    @NotNull
    public static final String CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS " + TableConst.DB_NAME + "." + TableConst.USERS + " (\n" +
            "`" + FieldConst.USER_ID + "` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.LOGIN + "` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.PASSWORD_HASH + "` VARCHAR(32) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.ROLE_TYPE + "` ENUM('USER','ADMIN') NOT NULL DEFAULT 'USER' COLLATE 'utf8_general_ci',\n" +
            "PRIMARY KEY (`" + FieldConst.USER_ID + "`) USING BTREE,\n" +
            "UNIQUE INDEX `" + FieldConst.LOGIN + "` (`" + FieldConst.LOGIN + "`) USING BTREE\n" +
            ")\n" +
            "COLLATE='utf8_general_ci'\n" +
            "ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
            ";";

    @NotNull
    public static final String CREATE_SESSIONS_TABLE = "CREATE TABLE IF NOT EXISTS " + TableConst.DB_NAME + "." + TableConst.SESSIONS + "(\n" +
            "`" + FieldConst.SESSION_ID + "` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.USER_ID + "` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.ROLE_TYPE + "` ENUM('USER','ADMIN') NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.SIGNATURE + "` VARCHAR(32) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.TIMESTAMP + "` BIGINT(20) NOT NULL,\n" +
            "PRIMARY KEY (`" + FieldConst.SESSION_ID + "`),\n" +
            "CONSTRAINT `FK__session_table_user_table` FOREIGN KEY (`" + FieldConst.USER_ID + "`) REFERENCES `" + TableConst.DB_NAME + "`.`" + TableConst.USERS + "` (`" + FieldConst.USER_ID + "`) ON UPDATE CASCADE ON DELETE CASCADE\n" +
            ")\n" +
            "COLLATE='utf8_general_ci'\n" +
            "ENGINE=InnoDB  DEFAULT CHARSET=utf8;\n" +
            ";";

    @NotNull
    public static final String CREATE_PROJECTS_TABLE = "CREATE TABLE IF NOT EXISTS `" + TableConst.DB_NAME + "`.`" + TableConst.PROJECTS + "` (\n" +
            "`" + FieldConst.PROJECT_ID + "` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.USER_ID + "` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.SHORT_LINK + "` SMALLINT NOT NULL AUTO_INCREMENT,\n" +
            "`" + FieldConst.NAME + "` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.DESCRIPTION + "` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.DATE_START + "` DATETIME NOT NULL,\n" +
            "`" + FieldConst.DATE_FINISH + "` DATETIME NOT NULL,\n" +
            "`status_field` ENUM('PLANNED','IN_PROGRESS','READY') NOT NULL DEFAULT 'PLANNED' COLLATE 'utf8_general_ci'," +
            "PRIMARY KEY (`" + FieldConst.PROJECT_ID + "`) USING BTREE,\n" +
            "UNIQUE INDEX `" + FieldConst.SHORT_LINK + "` (`" + FieldConst.SHORT_LINK + "`) USING BTREE,\n" +
            "INDEX `FK__user_table` (`" + FieldConst.USER_ID + "`) USING BTREE,\n" +
            "CONSTRAINT `FK__user_table_project_table` FOREIGN KEY (`" + FieldConst.USER_ID + "`) REFERENCES `" + TableConst.DB_NAME + "`.`" + TableConst.USERS + "` (`" + FieldConst.USER_ID + "`) ON UPDATE NO ACTION ON DELETE CASCADE\n" +
            ")\n" +
            "COLLATE='utf8_general_ci'\n" +
            "ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
            ";";

    @NotNull
    public static final String CREATE_TASKS_TABLE = "CREATE TABLE IF NOT EXISTS " + TableConst.DB_NAME + "." + TableConst.TASKS + " (\n" +
            "`" + FieldConst.TASK_ID + "` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.PROJECT_ID + "` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.USER_ID + "` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.SHORT_LINK + "` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,\n" +
            "`" + FieldConst.NAME + "` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.DESCRIPTION + "` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',\n" +
            "`" + FieldConst.DATE_START + "` DATETIME NOT NULL,\n" +
            "`" + FieldConst.DATE_FINISH + "` DATETIME NOT NULL,\n" +
            "`" + FieldConst.STATUS + "` ENUM('PLANNED','IN_PROGRESS','READY') NOT NULL DEFAULT 'PLANNED' COLLATE 'utf8_general_ci',\n" +
            "PRIMARY KEY (`" + FieldConst.TASK_ID + "`) USING BTREE,\n" +
            "UNIQUE INDEX `" + FieldConst.SHORT_LINK + "` (`" + FieldConst.SHORT_LINK + "`) USING BTREE,\n" +
            "INDEX `FK_task_table_user_table` (`" + FieldConst.USER_ID + "`) USING BTREE,\n" +
            "INDEX `FK_task_table_project_table` (`" + FieldConst.PROJECT_ID + "`) USING BTREE,\n" +
            "CONSTRAINT `FK_task_table_user_table` FOREIGN KEY (`" + FieldConst.USER_ID + "`) REFERENCES `" + TableConst.DB_NAME + "`.`" + TableConst.USERS + "` (`" + FieldConst.USER_ID + "`) ON UPDATE NO ACTION ON DELETE CASCADE,\n" +
            "CONSTRAINT `FK_task_table_project_table` FOREIGN KEY (`" + FieldConst.PROJECT_ID + "`) REFERENCES `" + TableConst.DB_NAME + "`.`" + TableConst.PROJECTS + "` (`" + FieldConst.PROJECT_ID + "`) ON UPDATE NO ACTION ON DELETE CASCADE\n" +
            ")\n" +
            "COLLATE='utf8_general_ci'\n" +
            "ENGINE=InnoDB  DEFAULT CHARSET=utf8;\n" +
            ";";
}
