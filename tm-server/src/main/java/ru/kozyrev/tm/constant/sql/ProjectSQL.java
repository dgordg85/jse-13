package ru.kozyrev.tm.constant.sql;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.db.TableConst;
import ru.kozyrev.tm.constant.entity.EntityMaskConst;

public class ProjectSQL {
    @NotNull
    private static final String DB_NAME = TableConst.DB_NAME;

    @NotNull
    private static final String TABLE_NAME = TableConst.PROJECTS;

    @NotNull
    public static final String FIND_ALL = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " ORDER BY " +
            FieldConst.SHORT_LINK;

    @NotNull
    public static final String FIND_ONE_BY_SHORT_LINK = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.SHORT_LINK + "=" + EntityMaskConst.SHORT_LINK + " AND " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID;
    @NotNull
    public static final String FIND_ALL_BY_USER_ID = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID;
    @NotNull
    public static final String FIND_ALL_BY_NUM = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " ORDER BY " +
            FieldConst.SHORT_LINK;
    @NotNull
    public static final String FIND_ALL_BY_START = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " ORDER BY " +
            FieldConst.DATE_START + ", " + FieldConst.SHORT_LINK;
    @NotNull
    public static final String FIND_ALL_BY_FINISH = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " ORDER BY " +
            FieldConst.DATE_FINISH + ", " + FieldConst.SHORT_LINK;
    @NotNull
    public static final String FIND_ALL_BY_STATUS = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " ORDER BY " +
            FieldConst.STATUS + ", " + FieldConst.SHORT_LINK;
    @NotNull
    public static final String FIND_ALL_BY_NUM_DESC = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " ORDER BY " +
            FieldConst.SHORT_LINK + " DESC";
    @NotNull
    public static final String FIND_ALL_BY_START_DESC = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " ORDER BY " +
            FieldConst.DATE_START + " DESC, " + FieldConst.SHORT_LINK + " DESC";
    @NotNull
    public static final String FIND_ALL_BY_FINISH_DESC = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " ORDER BY " +
            FieldConst.DATE_FINISH + " DESC, " + FieldConst.SHORT_LINK + " DESC";
    @NotNull
    public static final String FIND_ALL_BY_STATUS_DESC = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " ORDER BY " +
            FieldConst.STATUS + " DESC, " + FieldConst.SHORT_LINK + " DESC";
    @NotNull
    public static final String REMOVE_ALL_BY_USER_ID = "DELETE FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID;

    @NotNull
    public static final String REMOVE_ALL = "DELETE FROM " +
            DB_NAME + "." + TABLE_NAME;
    @NotNull
    public static final String FIND_WORD = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + " AND (" +
            FieldConst.NAME + " LIKE " + EntityMaskConst.WORD + " OR " +
            FieldConst.DESCRIPTION + " LIKE " + EntityMaskConst.WORD + ") ORDER BY " +
            FieldConst.SHORT_LINK;
    @NotNull
    private static final String ID = FieldConst.PROJECT_ID;
    @NotNull
    public static final String PERSIST = "INSERT INTO " +
            DB_NAME + "." + TABLE_NAME + " (" +
            ID + ", " + FieldConst.USER_ID + ", " + FieldConst.NAME + ", " +
            FieldConst.DESCRIPTION + ", " + FieldConst.DATE_START + ", " +
            FieldConst.DATE_FINISH + ", " + FieldConst.STATUS + ") VALUES (" +
            EntityMaskConst.ID + ", " + EntityMaskConst.USER_ID + ", " + EntityMaskConst.NAME + ", " +
            EntityMaskConst.DESCRIPTION + ", " + EntityMaskConst.DATE_START + ", " +
            EntityMaskConst.DATE_FINISH + ", " + EntityMaskConst.STATUS + ")";

    @NotNull
    public static final String MERGE = "UPDATE " +
            DB_NAME + "." + TABLE_NAME + " SET " +
            ID + "=" + EntityMaskConst.ID + ", " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + ", " +
            FieldConst.NAME + "=" + EntityMaskConst.NAME + ", " +
            FieldConst.DESCRIPTION + "=" + EntityMaskConst.DESCRIPTION + ", " +
            FieldConst.DATE_START + "=" + EntityMaskConst.DATE_START + ", " +
            FieldConst.DATE_FINISH + "=" + EntityMaskConst.DATE_FINISH + ", " +
            FieldConst.STATUS + "=" + EntityMaskConst.STATUS + " WHERE " +
            ID + "=" + EntityMaskConst.ID;

    @NotNull
    public static final String FIND_ONE = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            ID + "=" + EntityMaskConst.ID + " AND " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID;

    @NotNull
    public static final String REMOVE = "DELETE FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            ID + "=" + EntityMaskConst.ID + " AND " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + "";

    @NotNull
    public static final String RESET_INCREMENT = "ALTER TABLE " +
            DB_NAME + "." + TABLE_NAME + " AUTO_INCREMENT=1";
}
