package ru.kozyrev.tm.constant.db;

import org.jetbrains.annotations.NotNull;

public final class FieldConst {
    @NotNull
    public final static String DATE_START = "date_start_field";

    @NotNull
    public final static String DATE_FINISH = "date_finish_field";

    @NotNull
    public final static String DESCRIPTION = "description_field";

    @NotNull
    public final static String LOGIN = "login_field";

    @NotNull
    public final static String NAME = "name_field";

    @NotNull
    public final static String PASSWORD_HASH = "password_hash_field";

    @NotNull
    public final static String PROJECT_ID = "project_id_field";

    @NotNull
    public final static String ROLE_TYPE = "role_type_field";

    @NotNull
    public final static String SESSION_ID = "session_id_field";

    @NotNull
    public final static String SHORT_LINK = "short_link_field";

    @NotNull
    public final static String SIGNATURE = "signature_field";

    @NotNull
    public final static String STATUS = "status_field";

    @NotNull
    public final static String TASK_ID = "task_id_field";

    @NotNull
    public final static String TIMESTAMP = "timestamp_field";

    @NotNull
    public final static String USER_ID = "user_id_field";
}
