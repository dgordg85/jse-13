package ru.kozyrev.tm.constant.sql;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.db.TableConst;
import ru.kozyrev.tm.constant.entity.EntityMaskConst;

public class SessionSQL {
    @NotNull
    public static final String DB_NAME = TableConst.DB_NAME;

    @NotNull
    public static final String TABLE_NAME = TableConst.SESSIONS;

    @NotNull
    public static final String ID = FieldConst.SESSION_ID;

    @NotNull
    public static final String PERSIST = "INSERT INTO " +
            DB_NAME + "." + TABLE_NAME + "(" +
            ID + ", " + FieldConst.USER_ID + ", " +
            FieldConst.ROLE_TYPE + ", " + FieldConst.SIGNATURE + ", " + FieldConst.TIMESTAMP + ") VALUES (" +
            EntityMaskConst.ID + ", " + EntityMaskConst.USER_ID + ", " +
            EntityMaskConst.ROLE_TYPE + ", " + EntityMaskConst.SIGNATURE + ", " + EntityMaskConst.TIMESTAMP + ")";

    @NotNull
    public static final String MERGE = "UPDATE " +
            DB_NAME + "." + TABLE_NAME + " SET " +
            ID + "=" + EntityMaskConst.ID + ", " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID + ", " +
            FieldConst.ROLE_TYPE + "=" + EntityMaskConst.ROLE_TYPE + " WHERE " +
            ID + "=" + EntityMaskConst.ID;

    @NotNull
    public static final String FIND_ONE = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            ID + "=" + EntityMaskConst.ID;

    @NotNull
    public static final String FIND_ONE_BY_USER_ID = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.USER_ID + "=" + EntityMaskConst.USER_ID;

    @NotNull
    public static final String FIND_ALL = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME;

    @NotNull
    public static final String REMOVE = "DELETE FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            ID + "=" + EntityMaskConst.ID;

    @NotNull
    public static final String REMOVE_ALL = "DELETE FROM " +
            DB_NAME + "." + TABLE_NAME;
}
