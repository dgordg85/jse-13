package ru.kozyrev.tm.constant.db;

import org.jetbrains.annotations.NotNull;

public final class TableConst {
    @NotNull
    public final static String DB_NAME = "task_manager_db";

    @NotNull
    public final static String USERS = "users_table";

    @NotNull
    public final static String SESSIONS = "sessions_table";

    @NotNull
    public final static String PROJECTS = "projects_table";

    @NotNull
    public final static String TASKS = "tasks_table";
}
