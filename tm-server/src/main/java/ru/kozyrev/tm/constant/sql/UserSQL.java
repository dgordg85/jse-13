package ru.kozyrev.tm.constant.sql;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.constant.db.FieldConst;
import ru.kozyrev.tm.constant.db.TableConst;
import ru.kozyrev.tm.constant.entity.EntityMaskConst;

public class UserSQL {
    @NotNull
    public static final String DB_NAME = TableConst.DB_NAME;

    @NotNull
    public static final String TABLE_NAME = TableConst.USERS;

    @NotNull
    public static final String ID = FieldConst.USER_ID;

    @NotNull
    public static final String PERSIST = "INSERT INTO " +
            DB_NAME + "." + TABLE_NAME + " (" +
            ID + ", " + FieldConst.LOGIN + ", " + FieldConst.PASSWORD_HASH + ", " + FieldConst.ROLE_TYPE + ") VALUES (" +
            EntityMaskConst.ID + ", " + EntityMaskConst.LOGIN + ", " + EntityMaskConst.PASSWORD_HASH + ", " + EntityMaskConst.ROLE_TYPE + ")";

    @NotNull
    public static final String MERGE = "UPDATE " +
            DB_NAME + "." + TABLE_NAME + " SET " +
            ID + "=" + EntityMaskConst.ID + ", " +
            FieldConst.LOGIN + "=" + EntityMaskConst.LOGIN + ", " +
            FieldConst.PASSWORD_HASH + "=" + EntityMaskConst.PASSWORD_HASH + ", " +
            FieldConst.ROLE_TYPE + "=" + EntityMaskConst.ROLE_TYPE + " WHERE " +
            ID + "=" + EntityMaskConst.ID + "";

    @NotNull
    public static final String FIND_ONE = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            ID + "=" + EntityMaskConst.ID;

    @NotNull
    public static final String FIND_ONE_BY_LOGIN = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            FieldConst.LOGIN + "=" + EntityMaskConst.LOGIN;

    @NotNull
    public static final String FIND_ALL = "SELECT * FROM " +
            DB_NAME + "." + TABLE_NAME;

    @NotNull
    public static final String REMOVE_ALL = "DELETE FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE login_field <> 'admin'";

    @NotNull
    public static final String REMOVE = "DELETE FROM " +
            DB_NAME + "." + TABLE_NAME + " WHERE " +
            ID + "=" + EntityMaskConst.ID;


}
