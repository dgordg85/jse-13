package ru.kozyrev.tm.constant.entity;

import org.jetbrains.annotations.NotNull;

public class EntityMaskConst {
    @NotNull
    public final static String ID = "#{id}";

    @NotNull
    public final static String USER_ID = "#{userId}";

    @NotNull
    public final static String NAME = "#{name}";

    @NotNull
    public final static String DESCRIPTION = "#{description}";

    @NotNull
    public final static String DATE_START = "#{dateStart}";

    @NotNull
    public final static String DATE_FINISH = "#{dateFinish}";

    @NotNull
    public final static String STATUS = "#{status}";

    @NotNull
    public final static String SHORT_LINK = "#{shortLink}";

    @NotNull
    public final static String TIMESTAMP = "#{timestamp}";

    @NotNull
    public final static String ROLE_TYPE = "#{roleType}";

    @NotNull
    public final static String SIGNATURE = "#{signature}";

    @NotNull
    public final static String PROJECT_ID = "#{projectId}";

    @NotNull
    public final static String LOGIN = "#{login}";

    @NotNull
    public final static String PASSWORD_HASH = "#{passwordHash}";

    @NotNull
    public final static String WORD = "#{word}";

}