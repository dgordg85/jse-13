package ru.kozyrev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.exception.enumerated.UnknownTypeException;

@Getter
@RequiredArgsConstructor
public enum Direction {
    ASC("asc"),
    DESC("desc");

    @NotNull
    private final String displayName;

    @NotNull
    public final static Direction getType(@Nullable final String pType) throws UnknownTypeException {
        for (final Direction type : Direction.values()) {
            if (type.getDisplayName().equals(pType)) {
                return type;
            }
        }
        throw new UnknownTypeException();
    }
}
