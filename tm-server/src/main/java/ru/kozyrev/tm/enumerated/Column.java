package ru.kozyrev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.exception.enumerated.UnknownTypeException;

@Getter
@RequiredArgsConstructor
public enum Column {
    NUM("num"),
    DATE_START("dateStart"),
    DATE_FINISH("dateFinish"),
    STATUS("status");

    @NotNull
    private final String displayName;

    @NotNull
    public final static Column getType(@Nullable final String pType) throws UnknownTypeException {
        for (final Column type : Column.values()) {
            if (type.getDisplayName().equals(pType)) {
                return type;
            }
        }
        throw new UnknownTypeException();
    }
}
