package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IObjectService;
import ru.kozyrev.tm.entity.AbstractObject;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.util.StringUtil;

import java.sql.SQLException;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractObjectService<T extends AbstractObject> extends AbstractService<T> implements IObjectService<T> {
    @Nullable
    @Override
    public abstract T persist(@Nullable final T object) throws Exception;

    @Nullable
    @Override
    public final T persist(
            @Nullable final T object,
            @Nullable final String userId
    ) throws Exception {
        if (object == null) {
            throw new EmptyEntityException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        object.setUserId(userId);
        return this.persist(object);
    }

    @Nullable
    @Override
    public abstract T merge(@Nullable final T object, @Nullable final String userId) throws Exception;

    @Nullable
    @Override
    public final T findOne(
            @Nullable final String id,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (id == null || id.isEmpty()) {
            return null;
        }
        try (@NotNull final SqlSession session = getSession(true)) {
            return this.getMapper(session).findOne(id, userId);
        }
    }

    @Nullable
    @Override
    public final List<T> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        try (@NotNull final SqlSession session = getSession(true)) {
            return this.getMapper(session).findAllByUserId(userId);
        }
    }

    @Override
    public final void remove(@Nullable final String objectId, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (objectId == null || objectId.isEmpty()) {
            throw new EmptyEntityException();
        }
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).remove(objectId, userId);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).removeAll();
            this.getMapper(session).resetIncrement();
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).removeAllByUserId(userId);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Nullable
    @Override
    public List<T> findWord(
            @Nullable final String word,
            @Nullable final String userId
    ) throws Exception {
        if (word == null || word.isEmpty()) {
            return null;
        }
        @NotNull final String likeWord = "%" + word + "%";
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        try (@NotNull final SqlSession session = getSession(true)) {
            return this.getMapper(session).findWord(likeWord, userId);
        }
    }

    @Nullable
    @Override
    public final String getEntityIdByShortLink(
            @Nullable final String num,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final Integer shortLink = StringUtil.parseToInt(num);
        @Nullable final T entity;
        try (@NotNull final SqlSession session = getSession(true)) {
            entity = this.getMapper(session).findOneByShortLink(shortLink, userId);
        }
        if (entity == null) {
            throw new IndexException();
        }
        return entity.getId();
    }

    @Override
    public void removeEntityByShortLink(
            @Nullable final String num,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final String entityId = getEntityIdByShortLink(num, userId);
        if (entityId == null) {
            throw new IndexException();
        }
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).remove(entityId, userId);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        } finally {
            session.close();
        }
    }
}
