package ru.kozyrev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.IMapper;
import ru.kozyrev.tm.api.service.IService;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.data.NoDataException;

import java.sql.SQLException;
import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {
    @NotNull
    protected ServiceLocator serviceLocator;

    @Nullable
    @Override
    public abstract T persist(@Nullable final T entity) throws Exception;

    @Nullable
    @Override
    public final List<T> findAll() throws Exception {
        try (@NotNull final SqlSession session = getSession(true)) {
            return this.getMapper(session).findAll();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).removeAll();
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public void persist(@Nullable final List<T> entities) throws Exception {
        if (entities == null) {
            throw new NoDataException();
        }
        @NotNull final SqlSession session = getSession();
        try {
            for (final T entity : entities) {
                if (entity == null) {
                    continue;
                }
                this.getMapper(session).persist(entity);
            }
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract IMapper<T> getMapper(@NotNull final SqlSession session);

    @NotNull
    public final SqlSession getSession() {
        return serviceLocator.getSqlSessionFactory().openSession(false);
    }

    @NotNull
    public final SqlSession getSession(final boolean isAutocommit) {
        return serviceLocator.getSqlSessionFactory().openSession(isAutocommit);
    }
}
