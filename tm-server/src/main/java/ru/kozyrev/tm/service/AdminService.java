package ru.kozyrev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IAdminService;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.dto.DataTransferObject;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.exception.data.NoDataException;
import ru.kozyrev.tm.exception.enumerated.UnknownTypeException;
import ru.kozyrev.tm.util.PathUtil;

import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

@NoArgsConstructor
public final class AdminService implements IAdminService {
    @NotNull
    protected ServiceLocator serviceLocator;

    @Override
    public final void binSave() throws Exception {
        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.JAVA, DataFormat.BIN);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);
        @NotNull final ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(dto);

        oos.close();
        fos.close();
    }

    @Override
    public final void binLoad(@Nullable final Session session) throws Exception {
        @NotNull final String path = PathUtil.getPath(SerializeType.JAVA, DataFormat.BIN);
        try {
            @NotNull final FileInputStream fis = new FileInputStream(path);
            @NotNull final ObjectInputStream ois = new ObjectInputStream(fis);
            @NotNull final DataTransferObject dto = (DataTransferObject) ois.readObject();

            fis.close();
            ois.close();

            dto.unLoad(serviceLocator);
        } catch (final IOException e) {
            throw new NoDataException();
        }
    }

    @Override
    public final void jsonLoadFasterXML(@Nullable final Session session) throws Exception {
        @NotNull final String path = PathUtil.getPath(SerializeType.FASTER_XML, DataFormat.JSON);
        @NotNull final FileInputStream fis = new FileInputStream(path);

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final DataTransferObject dto = mapper.readValue(fis, DataTransferObject.class);
        dto.unLoad(serviceLocator);
        fis.close();
    }

    @Override
    public final void jsonSaveFasterXML() throws Exception {
        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.FASTER_XML, DataFormat.JSON);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(fos, dto);
        fos.close();
    }

    @Override
    public final void jsonSaveJaxB() throws Exception {
        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.JAX_B, DataFormat.JSON);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = (JAXBContext) JAXBContext.newInstance(DataTransferObject.class);

        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(dto, fos);
        fos.close();
    }

    @Override
    public final void jsonLoadJaxB(@Nullable final Session session) throws Exception {
        @NotNull final String path = PathUtil.getPath(SerializeType.JAX_B, DataFormat.JSON);
        @NotNull final StreamSource in = new StreamSource(new File(path));

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = (JAXBContext) JAXBContext.newInstance(DataTransferObject.class);
        @NotNull final Unmarshaller unMarshaller = context.createUnmarshaller();
        unMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");

        final DataTransferObject dto = (DataTransferObject) unMarshaller.unmarshal(in);
        dto.unLoad(serviceLocator);
    }

    @Override
    public final void xmlLoadFasterXML(@Nullable final Session session) throws Exception {
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        @NotNull final XmlMapper mapper = new XmlMapper(module);

        @NotNull final String path = PathUtil.getPath(SerializeType.FASTER_XML, DataFormat.XML);
        @NotNull final FileInputStream fis = new FileInputStream(path);

        @NotNull final DataTransferObject dto = mapper.readValue(fis, DataTransferObject.class);
        dto.unLoad(serviceLocator);
        fis.close();
    }

    @Override
    public final void xmlSaveFasterXML() throws Exception {
        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.FASTER_XML, DataFormat.XML);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final JacksonXmlModule module = new JacksonXmlModule();

        @NotNull final XmlMapper mapper = new XmlMapper(module);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mapper.writeValue(fos, dto);
        fos.close();
    }

    @Override
    public final void xmlSaveJaxB() throws Exception {
        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.JAX_B, DataFormat.XML);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = (JAXBContext) JAXBContext.newInstance(DataTransferObject.class);

        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/xml");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(dto, fos);
        fos.close();
    }

    @Override
    public final void xmlLoadJaxB(@Nullable final Session session) throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = (JAXBContext) JAXBContext.newInstance(DataTransferObject.class);
        @NotNull final Unmarshaller unMarshaller = context.createUnmarshaller();

        @NotNull final String path = PathUtil.getPath(SerializeType.JAX_B, DataFormat.XML);
        @NotNull final DataTransferObject dto = (DataTransferObject) unMarshaller.unmarshal(new File(path));

        dto.unLoad(serviceLocator);
    }

    @NotNull
    @Override
    public final String getDocumentStatusStr(@Nullable final String value) throws Exception {
        if (value == null || value.isEmpty()) {
            throw new UnknownTypeException();
        }
        return DocumentStatus.valueOf(value).getDisplayName();
    }

    @NotNull
    @Override
    public final String getRoleTypeStr(@Nullable final String value) throws Exception {
        if (value == null || value.isEmpty()) {
            throw new UnknownTypeException();
        }
        return RoleType.valueOf(value).getDisplayName();
    }

    @Override
    public final void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
