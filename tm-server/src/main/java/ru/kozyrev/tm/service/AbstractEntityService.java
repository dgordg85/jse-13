package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IEntityService;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.util.HashUtil;

import java.sql.SQLException;

@NoArgsConstructor
public abstract class AbstractEntityService<T extends AbstractEntity> extends AbstractService<T> implements IEntityService<T> {
    @Nullable
    @Override
    public final T findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IndexException();
        }
        try (@NotNull final SqlSession session = getSession(true)) {
            return this.getMapper(session).findOne(id);
        }
    }

    @Nullable
    @Override
    public abstract T persist(@Nullable final T entity) throws Exception;

    @Nullable
    @Override
    public abstract T merge(@Nullable final T entity) throws Exception;

    @Override
    public final void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).remove(id);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        } finally {
            session.close();
        }
    }

    protected final boolean isPassEmpty(@Nullable final String hashPassword) {
        return hashPassword == null || hashPassword.isEmpty() || hashPassword.equals(HashUtil.EMPTY_PASSWORD);
    }
}
