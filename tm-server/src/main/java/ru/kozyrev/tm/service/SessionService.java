package ru.kozyrev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.ISessionMapper;
import ru.kozyrev.tm.api.service.ISessionService;
import ru.kozyrev.tm.constant.ServerConst;
import ru.kozyrev.tm.entity.Session;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.session.SessionCloseException;
import ru.kozyrev.tm.exception.session.SessionCreateException;
import ru.kozyrev.tm.exception.session.SessionNotValidException;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginNotRegistryException;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;
import ru.kozyrev.tm.util.AesUtil;
import ru.kozyrev.tm.util.HashUtil;
import ru.kozyrev.tm.util.SignatureUtil;

import java.sql.SQLException;
import java.util.UUID;

public final class SessionService extends AbstractEntityService<Session> implements ISessionService {
    @Nullable
    @Override
    public final Session persist(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionCreateException();
        }
        if (session.getId() == null || session.getId().isEmpty()) {
            session.setId(UUID.randomUUID().toString());
        }
        if (session.getRoleType() == null) {
            throw new EmptyEntityException();
        }
        if (session.getUserId() == null || session.getUserId().isEmpty()) {
            throw new EmptyEntityException();
        }
        if (session.getTimestamp() == null) {
            throw new EmptyEntityException();
        }
        if (session.getSignature() == null || session.getSignature().isEmpty()) {
            throw new EmptyEntityException();
        }
        @NotNull final SqlSession SQLsession = getSession();
        try {
            this.getMapper(SQLsession).persist(session);
            SQLsession.commit();
        } catch (final SQLException e) {
            SQLsession.rollback();
        }
        @Nullable final Session result = this.getMapper(SQLsession).findOne(session.getId());
        SQLsession.close();
        return result;
    }

    @Nullable
    @Override
    public final Session merge(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new SessionNotValidException();
        }
        if (session.getId() == null) {
            throw new EmptyEntityException();
        }
        @Nullable final Session sessionDB = findOne(session.getId());
        if (sessionDB == null) {
            return this.persist(session);
        }
        if (session.getTimestamp() != null) {
            sessionDB.setTimestamp(session.getTimestamp());
        }
        if (session.getRoleType() != null) {
            sessionDB.setRoleType(session.getRoleType());
        }
        if (session.getUserId() != null && session.getUserId().isEmpty()) {
            sessionDB.setUserId(session.getUserId());
        }
        @NotNull final Session sessionSign = SignatureUtil.signSession(sessionDB);
        @NotNull final SqlSession SQLsession = getSession();
        try {
            this.getMapper(SQLsession).merge(sessionSign);
            SQLsession.commit();
        } catch (final SQLException e) {
            SQLsession.rollback();
        }
        @Nullable final Session result = this.getMapper(SQLsession).findOne(session.getId());
        SQLsession.close();
        return result;

    }

    @NotNull
    @Override
    public final String openSession(
            @Nullable final String login,
            @Nullable final String hashPassword
    ) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (isPassEmpty(hashPassword)) {
            throw new UserPasswordEmptyException();
        }
        @Nullable final User user = serviceLocator.getUserService().getUserByLogin(login);
        if (user == null) {
            throw new UserLoginNotRegistryException();
        }
        @NotNull final String saltHashPassword = HashUtil.getCycleHash(hashPassword);
        if (!saltHashPassword.equals(user.getPasswordHash())) {
            throw new UserPasswordWrongException();
        }
        @Nullable Session session = getSessionByUserId(user.getId());
        if (!isSessionAlive(session)) {
            session = null;
        }
        if (session == null) {
            @NotNull final Session newSession = createSession(user);
            this.persist(newSession);
            session = this.findOne(newSession.getId());
            if (session == null) {
                throw new SessionCreateException();
            }
        }
        @Nullable final String sessionBASE64 = AesUtil.encryptSession(session);
        if (sessionBASE64 == null) {
            throw new SessionCreateException();
        }
        return sessionBASE64;
    }

    @Override
    public final Session validateSession(@Nullable final String sessionBASE64) throws Exception {
        @Nullable final Session session = AesUtil.decryptSession(sessionBASE64);
        if (isSessionExist(session) && isSessionAlive(session)) {
            return session;
        }
        return null;
    }

    private final boolean isSessionExist(@Nullable final Session session) throws Exception {
        if (session == null) {
            return false;
        }
        if (session.getId() == null || session.getId().isEmpty()) {
            throw new SessionNotValidException();
        }
        @Nullable final Session serverSession;
        try (@NotNull final SqlSession SQLsession = getSession(true)) {
            serverSession = this.getMapper(SQLsession).findOne(session.getId());
        }
        if (serverSession == null) {
            return false;
        }
        if (session.getSignature() == null || session.getSignature().isEmpty()) {
            return false;
        }
        return serverSession.equals(session);
    }

    private final boolean isSessionAlive(@Nullable final Session session) throws Exception {
        if (session == null) {
            return false;
        }
        if (session.getTimestamp() == null) {
            throw new SessionNotValidException();
        }
        if (session.getId() == null || session.getId().isEmpty()) {
            throw new SessionNotValidException();
        }
        final long sessionTime = System.currentTimeMillis() - session.getTimestamp();
        if (ServerConst.SESSION_LIFETIME < sessionTime) {
            @NotNull final SqlSession SQLsession = getSession();
            try {
                this.getMapper(SQLsession).remove(session.getId());
                SQLsession.commit();
            } catch (SQLException e) {
                SQLsession.rollback();
            } finally {
                SQLsession.close();
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean forbiddenSession(@Nullable final Session session) {
        if (session == null) {
            return false;
        }
        if (session.getRoleType() == null) {
            return false;
        }
        return session.getRoleType().equals(RoleType.ADMIN);
    }

    @Override
    public final void closeSession(@Nullable final String sessionBASE64) throws Exception {
        @Nullable final Session session = AesUtil.decryptSession(sessionBASE64);
        if (!isSessionExist(session)) {
            throw new SessionNotValidException();
        }
        @Nullable final String sessionId = session.getId();
        if (sessionId == null) {
            throw new SessionCloseException();
        }
        @NotNull final SqlSession SQLsession = getSession();
        try {
            this.getMapper(SQLsession).remove(sessionId);
            SQLsession.commit();
        } catch (final SQLException e) {
            SQLsession.rollback();
        } finally {
            SQLsession.close();
        }
    }

    @NotNull
    private final Session createSession(@NotNull final User user) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(UUID.randomUUID().toString());
        session.setUserId(user.getId());
        session.setRoleType(user.getRoleType());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Session sessionSign = SignatureUtil.signSession(session);
        return sessionSign;
    }

    @Nullable
    private final Session getSessionByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            return null;
        }
        try (@NotNull final SqlSession SQLsession = getSession(true)) {
            return this.getMapper(SQLsession).findOneByUserId(userId);
        }
    }

    @NotNull
    public final ISessionMapper getMapper(@NotNull final SqlSession session) {
        return session.getMapper(ISessionMapper.class);
    }
}
