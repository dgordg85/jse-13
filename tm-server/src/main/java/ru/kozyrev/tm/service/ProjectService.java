package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.IProjectMapper;
import ru.kozyrev.tm.api.service.IProjectService;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
public final class ProjectService extends AbstractObjectService<Project> implements IProjectService {
    @Nullable
    @Override
    public final Project persist(@Nullable final Project project) throws Exception {
        if (project == null) {
            throw new EmptyEntityException();
        }
        if (project.getId() == null || project.getId().isEmpty()) {
            project.setId(UUID.randomUUID().toString());
        }
        if (project.getUserId() == null || project.getUserId().isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new NameException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        if (project.getStatus() == null) {
            project.setStatus(DocumentStatus.PLANNED);
        }
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).persist(project);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        }
        @Nullable final Project result = this.getMapper(session).findOne(project.getId(), project.getId());
        session.close();
        return result;
    }

    @Nullable
    @Override
    public final Project merge(
            @Nullable final Project project,
            @Nullable final String userId
    ) throws Exception {
        if (project == null) {
            throw new ServiceFailException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (project.getId() == null) {
            throw new EntityException();
        }
        @Nullable final Project projectUpdate = this.findOne(project.getId(), userId);
        if (projectUpdate == null) {
            return persist(project, userId);
        }
        if (project.getName() != null && !project.getName().isEmpty()) {
            projectUpdate.setName(project.getName());
        }
        if (project.getDescription() != null && !project.getDescription().isEmpty()) {
            projectUpdate.setDescription(project.getDescription());
        }
        if (project.getDateStart() != null) {
            projectUpdate.setDateStart(project.getDateStart());
        }
        if (project.getDateFinish() != null) {
            projectUpdate.setDateFinish(project.getDateFinish());
        }
        if (project.getStatus() != null) {
            projectUpdate.setStatus(project.getStatus());
        }

        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).merge(projectUpdate);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        }
        @Nullable final Project result = this.getMapper(session).findOne(project.getId(), userId);
        session.close();
        return result;
    }

    @Nullable
    @Override
    public final List<Project> findAll(
            @NotNull final Column column,
            @NotNull final Direction direction,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final List<Project> list;
        try (@NotNull final SqlSession session = getSession(true)) {
            switch (column) {
                case DATE_START:
                    if (direction == Direction.DESC) {
                        list = this.getMapper(session).findAllByDateStartDesc(userId);
                    } else {
                        list = this.getMapper(session).findAllByDateStart(userId);
                    }

                    break;
                case DATE_FINISH:
                    if (direction == Direction.DESC) {
                        list = this.getMapper(session).findAllByDateFinishDesc(userId);
                    } else {
                        list = this.getMapper(session).findAllByDateFinish(userId);
                    }
                    break;
                case STATUS:
                    if (direction == Direction.DESC) {
                        list = this.getMapper(session).findAllByStatusDesc(userId);
                    } else {
                        list = this.getMapper(session).findAllByStatus(userId);
                    }
                    break;
                default:
                    if (direction == Direction.DESC) {
                        list = this.getMapper(session).findAllByNumDesc(userId);
                    } else {
                        list = this.getMapper(session).findAllByNum(userId);
                    }
            }
        }
        return list;
    }

    @NotNull
    public final IProjectMapper getMapper(@NotNull final SqlSession session) {
        return session.getMapper(IProjectMapper.class);
    }
}
