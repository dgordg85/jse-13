package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.mapper.ITaskMapper;
import ru.kozyrev.tm.api.service.ITaskService;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.exception.entity.*;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
public final class TaskService extends AbstractObjectService<Task> implements ITaskService {
    @Nullable
    @Override
    public final Task persist(@Nullable final Task task) throws Exception {
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getId() == null || task.getId().isEmpty()) {
            task.setId(UUID.randomUUID().toString());
        }
        if (task.getUserId() == null || task.getUserId().isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (task.getName() == null || task.getName().isEmpty()) {
            throw new NameException();
        }
        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
            throw new IndexException();
        }
        if (task.getDateStart() == null) {
            throw new DateException();
        }
        if (task.getDateFinish() == null) {
            throw new DateException();
        }
        if (task.getStatus() == null) {
            task.setStatus(DocumentStatus.PLANNED);
        }
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).persist(task);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        }
        @Nullable final Task result = this.getMapper(session).findOne(task.getId(), task.getUserId());
        session.close();
        return result;
    }

    @Nullable
    @Override
    public final Task merge(
            @Nullable final Task task,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (task == null) {
            throw new EmptyEntityException();
        }
        if (task.getId() == null || task.getId().isEmpty()) {
            throw new EntityException();
        }
        @Nullable final Task taskUpdate = this.findOne(task.getId(), userId);
        if (taskUpdate == null) {
            return persist(task, userId);
        }
        if (task.getName() != null && !task.getName().isEmpty()) {
            taskUpdate.setName(task.getName());
        }
        if (task.getProjectId() != null && !task.getProjectId().isEmpty()) {
            taskUpdate.setProjectId(task.getProjectId());
        }
        if (task.getDescription() != null && !task.getDescription().isEmpty()) {
            taskUpdate.setDescription(task.getDescription());
        }
        if (task.getDateStart() != null) {
            taskUpdate.setDateStart(task.getDateStart());
        }
        if (task.getDateFinish() != null) {
            taskUpdate.setDateFinish(task.getDateFinish());
        }
        if (task.getStatus() != null) {
            taskUpdate.setStatus(task.getStatus());
        }

        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).merge(taskUpdate);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        }
        @Nullable final Task result = this.getMapper(session).findOne(task.getId(), userId);
        session.close();
        return result;
    }

    @Nullable
    @Override
    public final List<Task> findAll(
            @Nullable final String projectId,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        try (@NotNull final SqlSession session = getSession(true)) {
            return this.getMapper(session).findAllByProjectId(projectId, userId);
        }
    }

    @Override
    public final void removeAll(
            @Nullable final String projectId,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        @NotNull final SqlSession session = getSession();
        try {
            this.getMapper(session).removeAllByProject(projectId, userId);
            session.commit();
        } catch (final SQLException e) {
            session.rollback();
        } finally {
            session.close();
        }
    }

    @Override
    public final void removeAllByProjectNum(
            @Nullable final String projectNum,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final String projectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
        this.removeAll(projectId, userId);
    }

    @Nullable
    @Override
    public final List<Task> findAll(
            @Nullable final String projectId,
            @NotNull final Column column,
            @NotNull final Direction direction,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new IndexException();
        }
        @Nullable final List<Task> list;
        try (@NotNull final SqlSession session = getSession(true)) {
            switch (column) {
                case DATE_START:
                    if (direction == Direction.DESC) {
                        list = this.getMapper(session).findAllByDateStartDesc(projectId, userId);
                    } else {
                        list = this.getMapper(session).findAllByDateStart(projectId, userId);
                    }

                    break;
                case DATE_FINISH:
                    if (direction == Direction.DESC) {
                        list = this.getMapper(session).findAllByDateFinishDesc(projectId, userId);
                    } else {
                        list = this.getMapper(session).findAllByDateFinish(projectId, userId);
                    }
                    break;
                case STATUS:
                    if (direction == Direction.DESC) {
                        list = this.getMapper(session).findAllByStatusDesc(projectId, userId);
                    } else {
                        list = this.getMapper(session).findAllByStatus(projectId, userId);
                    }
                    break;
                default:
                    if (direction == Direction.DESC) {
                        list = this.getMapper(session).findAllByNumDesc(projectId, userId);
                    } else {
                        list = this.getMapper(session).findAllByNum(projectId, userId);
                    }
            }
        }
        return list;
    }

    @NotNull
    public final ITaskMapper getMapper(@NotNull final SqlSession session) {
        return session.getMapper(ITaskMapper.class);
    }
}
