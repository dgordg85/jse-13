package ru.kozyrev.tm.exception.user;

public final class UserPasswordMatchException extends Exception {
    public UserPasswordMatchException() {
        super("Password don't match!");
    }
}
