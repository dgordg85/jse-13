package ru.kozyrev.tm.exception.command;

public final class CommandCorruptException extends Exception {
    public CommandCorruptException() {
        super("Command name or description is broken");
    }
}
