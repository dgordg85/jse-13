package ru.kozyrev.tm.exception.user;

public final class UserUpdateErrorException extends Exception {
    public UserUpdateErrorException() {
        super("Not Update! Check password and new login!");
    }
}
