package ru.kozyrev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@RequiredArgsConstructor
public enum DocumentStatus {
    PLANNED("Запланированно"),
    IN_PROGRESS("В процессе"),
    READY("Готово");

    @NotNull
    private final String displayName;
}
