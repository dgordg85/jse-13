package ru.kozyrev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 240;

    public HelpCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "help";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all commands.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[COMMANDS LIST]");
        System.out.println("DEFAULT USERS: user:user, admin:admin;");
        terminalService.printCommands();
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
