package ru.kozyrev.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class FasterXmlSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 63;

    public FasterXmlSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-xml-fasterXML";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data to XML file by FasterXML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[FasterXML XML SAVE]");
        @Nullable final String session = stateService.getSession();
        adminEndpoint.adminXmlSaveFasterXML(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
