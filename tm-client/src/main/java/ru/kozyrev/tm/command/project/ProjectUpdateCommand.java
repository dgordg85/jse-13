package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.DocumentStatus;
import ru.kozyrev.tm.api.endpoint.Project;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.DateUtil;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 24;

    public ProjectUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        @NotNull final Project project = new Project();

        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        @NotNull final String projectNum = terminalService.nextLine();

        @NotNull final String newProjectId = projectEndpoint.projectGetIdByShortLink(session, projectNum);
        project.setId(newProjectId);

        System.out.println("ENTER NAME:");
        project.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATE START:");
        project.setDateStart(DateUtil.parseDateXML(terminalService.nextLine()));

        System.out.println("ENTER DATE FINISH:");
        project.setDateFinish(DateUtil.parseDateXML(terminalService.nextLine()));

        System.out.println("STATUS: (plan / progress / ready)");
        @NotNull final String statusString = terminalService.nextLine();

        @Nullable DocumentStatus status = null;
        if (!statusString.isEmpty()) {
            status = DocumentStatus.fromValue(statusString);
        }
        project.setStatus(status);

        projectEndpoint.projectMergeAuth(session, project);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
