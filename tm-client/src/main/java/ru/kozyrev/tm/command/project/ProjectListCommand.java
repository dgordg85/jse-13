package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.Project;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 20;

    public ProjectListCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all projects.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("[PROJECTS LIST]");
        @NotNull final Direction direction = stateService.getProjectDirection();
        @NotNull final Column column = stateService.getProjectColumn();
        @Nullable final List<Project> list = projectEndpoint.projectFindAllBySorting(session, column, direction);
        printList(list);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
