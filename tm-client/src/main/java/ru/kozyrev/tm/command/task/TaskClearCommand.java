package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;


public final class TaskClearCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 42;

    public TaskClearCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("[CLEAR]");
        System.out.println("[TASK REMOVES]\nENTER PROJECT ID:");
        @NotNull final String projectNum = terminalService.nextLine();

        taskEndpoint.taskRemoveAllByProjectNumAuth(session, projectNum);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
