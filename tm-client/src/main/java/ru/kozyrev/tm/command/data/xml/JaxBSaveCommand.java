package ru.kozyrev.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class JaxBSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 69;

    public JaxBSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-xml-JaxB";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data from XML file by Jax-B";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[Jax-B XML SAVE]");
        @Nullable final String session = stateService.getSession();
        adminEndpoint.adminXmlSaveJaxB(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
