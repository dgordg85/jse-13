package ru.kozyrev.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class FasterXmlLoadCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 66;

    public FasterXmlLoadCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-load-json-fasterXML";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Load data from JSON file by FasterXML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[FasterXML JSON LOAD]");
        @Nullable final String session = stateService.getSession();
        adminEndpoint.adminJsonLoadFasterXML(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
