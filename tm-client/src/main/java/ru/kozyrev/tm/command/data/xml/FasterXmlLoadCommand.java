package ru.kozyrev.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class FasterXmlLoadCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 64;

    public FasterXmlLoadCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-load-xml-fasterXML";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Load data from XML file by FasterXML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[FasterXML XML LOAD]");
        @Nullable final String session = stateService.getSession();
        adminEndpoint.adminXmlLoadFasterXML(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
