package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class UserProfileCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 7;

    public UserProfileCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show user profile";
    }

    @Override
    public final void execute() {
        System.out.println("[USER PROFILE]");
        System.out.printf("Login: %s\n", stateService.getLogin());
        System.out.printf("Role: %s\n", stateService.getRoleStr());
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
