package ru.kozyrev.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 250;

    public AboutCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "about";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Print information about build";
    }

    @Override
    public final void execute() {
        @NotNull final String developer = Manifests.read("developer");
        @NotNull final String version = Manifests.read("version");
        System.out.printf("Developer: %s\nBuildNumber: %s\n", developer, version);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
