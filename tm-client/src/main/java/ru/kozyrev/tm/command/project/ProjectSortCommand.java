package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;


public final class ProjectSortCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 70;

    public ProjectSortCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "sort-project";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Using for set sorting option for Projects";
    }

    @Override
    public final void execute() {
        System.out.println("[SORT PROJECT]");
        System.out.println("'ASC' / 'DESC'?");
        @NotNull final String sortString = terminalService.nextLine();

        if (!sortString.isEmpty()) {
            @NotNull final Direction direction = Direction.valueOf(sortString.toUpperCase());
            stateService.setProjectDirection(direction);
        }

        System.out.println("Column for sorting? (num / date_Begin / date_End / status)");
        @NotNull final String columnStr = terminalService.nextLine();

        if (!columnStr.isEmpty()) {
            @NotNull final Column column = Column.valueOf(columnStr.toUpperCase());
            stateService.setProjectColumn(column);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
