package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class UserRemoveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 11;

    public UserRemoveCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for deleting profile";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("[USER REMOVE]");
        System.out.println("Are you sure? Type 'yes' or another for cancel...");
        @NotNull final String answer = terminalService.nextLine();
        if ("yes".equals(answer.toLowerCase())) {
            userEndpoint.userRemoveBySession(session);
            System.out.println("[USER DELETE!]");
            stateService.clearSession();
        } else {
            System.out.println("[OPERATION ABORT!]");
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
