package ru.kozyrev.tm.command.data.bin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;

public final class DataBinLoadCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 62;

    public DataBinLoadCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-load-bin";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Load data from Binary file";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SERIALIZATION BIN LOAD]");
        @Nullable final String session = stateService.getSession();
        adminEndpoint.adminBinLoad(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
