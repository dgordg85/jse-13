package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;


public final class TaskSortCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 75;

    public TaskSortCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "sort-task";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Using for set sorting option for Tasks";
    }

    @Override
    public void execute() {
        System.out.println("[SORT TASK]");
        System.out.println("'ASC' / 'DESC'?");
        @NotNull final String sortString = terminalService.nextLine().toLowerCase();

        if (!sortString.isEmpty()) {
            @NotNull final Direction direction = Direction.fromValue(sortString);
            stateService.setTaskDirection(direction);
        }

        System.out.println("Column for sorting? (num / dateBegin / dateEnd / Status)");
        @NotNull final String columnStr = terminalService.nextLine().toLowerCase();

        if (!columnStr.isEmpty()) {
            @NotNull final Column column = Column.fromValue(columnStr);
            stateService.setTaskColumn(column);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
