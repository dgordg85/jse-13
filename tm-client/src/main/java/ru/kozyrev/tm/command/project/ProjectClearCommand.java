package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;


public final class ProjectClearCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 28;

    public ProjectClearCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("[CLEAR]");
        projectEndpoint.projectRemoveAllAuth(session);
        System.out.println("[ALL PROJECTS AND TASKS REMOVED]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
