package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.Project;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.DateUtil;

public final class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 22;

    public ProjectCreateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Create new project.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        @NotNull final Project project = new Project();
        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        project.setName(terminalService.nextLine());

        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATE START:");
        project.setDateStart(DateUtil.parseDateXML(terminalService.nextLine()));

        System.out.println("ENTER DATE FINISH:");
        project.setDateFinish(DateUtil.parseDateXML(terminalService.nextLine()));

        projectEndpoint.projectPersistAuth(session, project);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
