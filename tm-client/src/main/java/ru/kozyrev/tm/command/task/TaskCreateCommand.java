package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.Task;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskCreateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 44;

    public TaskCreateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Create new tasks.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        @NotNull final Task task = new Task();

        System.out.println("[TASK CREATE]\nENTER NAME:");
        task.setName(terminalService.nextLine());

        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectNum = terminalService.nextLine();

        @NotNull final String projectId = projectEndpoint.projectGetIdByShortLink(session, projectNum);
        task.setProjectId(projectId);

        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATE START:");
        task.setDateStart(DateUtil.parseDateXML(terminalService.nextLine()));

        System.out.println("ENTER DATE FINISH:");
        task.setDateFinish(DateUtil.parseDateXML(terminalService.nextLine()));

        taskEndpoint.taskPersistAuth(session, task);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
