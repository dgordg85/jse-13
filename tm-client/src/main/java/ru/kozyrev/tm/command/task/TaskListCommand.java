package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.Task;
import ru.kozyrev.tm.command.AbstractCommand;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 40;

    public TaskListCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("INPUT ID PROJECT");
        @NotNull final String projectNum = terminalService.nextLine();

        System.out.println("[TASKS LIST OF PROJECT]");
        @NotNull final String projectId = projectEndpoint.projectGetIdByShortLink(session, projectNum);
        @NotNull final Direction direction = stateService.getTaskDirection();
        @NotNull final Column column = stateService.getTaskColumn();
        @Nullable final List<Task> list = taskEndpoint.taskFindAllByProjectAuthSort(session, projectId, column, direction);

        printList(list);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
