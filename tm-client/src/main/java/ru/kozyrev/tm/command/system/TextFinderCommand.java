package ru.kozyrev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.Project;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.Task;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.entity.EmptyStringException;

import java.util.List;

public final class TextFinderCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 95;

    public TextFinderCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "find";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Search words in Project/Tasks";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("[FIND]");
        System.out.println("Print word...");
        @NotNull final String word = terminalService.nextLine();
        if (word.isEmpty()) {
            throw new EmptyStringException();
        }

        @Nullable final List<Project> listProjects = projectEndpoint.projectFindWord(session, word);
        if (listProjects != null && listProjects.size() != 0) {
            System.out.println("[FIND PROJECTS]");
            printList(listProjects);
        }

        @Nullable final List<Task> listTasks = taskEndpoint.taskFindWord(session, word);
        if (listTasks != null && listTasks.size() != 0) {
            System.out.println("[FIND TASKS]");
            printList(listTasks);
        }
        if (listProjects != null && listTasks != null && listProjects.size() == 0 && listTasks.size() == 0) {
            System.out.printf("%s - not find!\n", word);
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
