package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.command.AbstractCommand;


public final class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 26;

    public ProjectRemoveCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("[PROJECT REMOVE]\nENTER ID:");
        @NotNull final String projectNum = terminalService.nextLine();

        projectEndpoint.projectRemoveByShortLink(session, projectNum);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
