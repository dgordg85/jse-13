package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.DocumentStatus;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.Task;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 46;

    public TaskUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update selected task.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        @NotNull final Task task = new Task();

        System.out.println("[UPDATE TASK]\nENTER ID:");
        @NotNull final String taskNum = terminalService.nextLine();

        @Nullable final String taskId = taskEndpoint.taskGetIdByShortLink(session, taskNum);
        task.setId(taskId);

        System.out.println("NEW NAME:");
        task.setName(terminalService.nextLine());

        System.out.println("NEW PROJECT ID:");
        @NotNull final String projectNum = terminalService.nextLine();

        if (!projectNum.isEmpty()) {
            @NotNull final String projectId = projectEndpoint.projectGetIdByShortLink(session, projectNum);
            task.setProjectId(projectId);
        } else {
            task.setProjectId("");
        }

        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(terminalService.nextLine());

        System.out.println("ENTER DATE START:");
        @NotNull final String dateBegin = terminalService.nextLine();

        if (!dateBegin.isEmpty()) {
            task.setDateStart(DateUtil.parseDateXML(dateBegin));
        } else {
            task.setDateStart(null);
        }

        System.out.println("ENTER DATE FINISH:");
        @NotNull final String dateFinish = terminalService.nextLine();

        if (!dateFinish.isEmpty()) {
            task.setDateFinish(DateUtil.parseDateXML(dateFinish));
        } else {
            task.setDateFinish(null);
        }

        System.out.println("STATUS: (plan / progress / ready)");
        @NotNull final String statusString = terminalService.nextLine();
        @Nullable DocumentStatus status = null;
        if (!statusString.isEmpty()) {
            status = DocumentStatus.valueOf(statusString);
        }
        task.setStatus(status);
        taskEndpoint.taskMergeAuth(session, task);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }

}
