package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.endpoint.User;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.user.LoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginTakenException;

public final class UserUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 9;

    public UserUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update user login.";
    }

    @Override
    public final void execute() throws Exception {
        @Nullable final String session = stateService.getSession();

        System.out.println("[UPDATE USER]");

        System.out.println("ENTER NEW LOGIN:");
        @NotNull final String login = terminalService.nextLine();
        if (login.isEmpty()) {
            throw new LoginEmptyException();
        }
        if (userEndpoint.userGetUserByLogin(session, login) != null) {
            throw new UserLoginTakenException();
        }
        @NotNull final User user = userEndpoint.userGetUserByLogin(session, stateService.getLogin());
        user.setLogin(login);
        user.setPasswordHash("");
        userEndpoint.userMerge(session, user);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
