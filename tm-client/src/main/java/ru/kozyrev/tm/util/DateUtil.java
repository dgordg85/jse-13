package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.exception.entity.DateException;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {
    @NotNull
    private final static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    @NotNull
    public final static String getDateXML(@NotNull final XMLGregorianCalendar date) {
        @NotNull final GregorianCalendar gCalendar = date.toGregorianCalendar();
        @NotNull final Date date2 = gCalendar.getTime();
        return formatter.format(date2);
    }

    public final static XMLGregorianCalendar parseDateXML(@Nullable final String date) throws Exception {
        if (date == null || date.isEmpty()) {
            throw new DateException();
        }
        @NotNull final GregorianCalendar c = new GregorianCalendar();
        c.setTime(formatter.parse(date));
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
    }
}
