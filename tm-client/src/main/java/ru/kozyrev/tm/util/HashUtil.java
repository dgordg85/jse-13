package ru.kozyrev.tm.util;

import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {
    @NotNull
    public final static String EMPTY_PASSWORD = "d41d8cd98f00b204e9800998ecf8427e";

    @NotNull
    public final static String getHash(@NotNull final String password) {
        @NotNull final StringBuilder sb = new StringBuilder();
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

            for (final byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
