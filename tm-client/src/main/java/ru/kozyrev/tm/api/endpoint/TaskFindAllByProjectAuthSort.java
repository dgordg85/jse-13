
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for taskFindAllByProjectAuthSort complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="taskFindAllByProjectAuthSort"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sessionBASE64" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="projectId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="arg2" type="{http://endpoint.api.tm.kozyrev.ru/}column" minOccurs="0"/&gt;
 *         &lt;element name="arg3" type="{http://endpoint.api.tm.kozyrev.ru/}direction" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taskFindAllByProjectAuthSort", propOrder = {
        "sessionBASE64",
        "projectId",
        "arg2",
        "arg3"
})
public class TaskFindAllByProjectAuthSort {

    protected String sessionBASE64;
    protected String projectId;
    @XmlSchemaType(name = "string")
    protected Column arg2;
    @XmlSchemaType(name = "string")
    protected Direction arg3;

    /**
     * Gets the value of the sessionBASE64 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSessionBASE64() {
        return sessionBASE64;
    }

    /**
     * Sets the value of the sessionBASE64 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSessionBASE64(final String value) {
        this.sessionBASE64 = value;
    }

    /**
     * Gets the value of the projectId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * Sets the value of the projectId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProjectId(final String value) {
        this.projectId = value;
    }

    /**
     * Gets the value of the arg2 property.
     *
     * @return
     *     possible object is
     *     {@link Column }
     *
     */
    public Column getArg2() {
        return arg2;
    }

    /**
     * Sets the value of the arg2 property.
     *
     * @param value
     *     allowed object is
     *     {@link Column }
     *
     */
    public void setArg2(final Column value) {
        this.arg2 = value;
    }

    /**
     * Gets the value of the arg3 property.
     *
     * @return
     *     possible object is
     *     {@link Direction }
     *
     */
    public Direction getArg3() {
        return arg3;
    }

    /**
     * Sets the value of the arg3 property.
     *
     * @param value
     *     allowed object is
     *     {@link Direction }
     *
     */
    public void setArg3(final Direction value) {
        this.arg3 = value;
    }

}
