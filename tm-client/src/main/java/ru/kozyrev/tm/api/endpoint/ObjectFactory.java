
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.kozyrev.tm.api.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.kozyrev.ru/", "Exception");
    private final static QName _CloseSession_QNAME = new QName("http://endpoint.api.tm.kozyrev.ru/", "closeSession");
    private final static QName _CloseSessionResponse_QNAME = new QName("http://endpoint.api.tm.kozyrev.ru/", "closeSessionResponse");
    private final static QName _OpenSession_QNAME = new QName("http://endpoint.api.tm.kozyrev.ru/", "openSession");
    private final static QName _OpenSessionResponse_QNAME = new QName("http://endpoint.api.tm.kozyrev.ru/", "openSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.kozyrev.tm.api.endpoint
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     *
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CloseSession }
     *
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     *
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     *
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     *
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kozyrev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(final Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kozyrev.ru/", name = "closeSession")
    public JAXBElement<CloseSession> createCloseSession(final CloseSession value) {
        return new JAXBElement<CloseSession>(_CloseSession_QNAME, CloseSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kozyrev.ru/", name = "closeSessionResponse")
    public JAXBElement<CloseSessionResponse> createCloseSessionResponse(final CloseSessionResponse value) {
        return new JAXBElement<CloseSessionResponse>(_CloseSessionResponse_QNAME, CloseSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kozyrev.ru/", name = "openSession")
    public JAXBElement<OpenSession> createOpenSession(final OpenSession value) {
        return new JAXBElement<OpenSession>(_OpenSession_QNAME, OpenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.kozyrev.ru/", name = "openSessionResponse")
    public JAXBElement<OpenSessionResponse> createOpenSessionResponse(final OpenSessionResponse value) {
        return new JAXBElement<OpenSessionResponse>(_OpenSessionResponse_QNAME, OpenSessionResponse.class, null, value);
    }

}
