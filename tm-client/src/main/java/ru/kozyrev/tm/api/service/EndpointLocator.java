package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.endpoint.*;

public interface EndpointLocator {
    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAdminEndpoint getAdminEndpoint();

    @NotNull
    ISessionEndpoint getSessionEndpoint();
}
