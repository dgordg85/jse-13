
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for user complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="user"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.api.tm.kozyrev.ru/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="passwordHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="roleType" type="{http://endpoint.api.tm.kozyrev.ru/}roleType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "user", propOrder = {
        "login",
        "passwordHash",
        "roleType"
})
public class User
        extends AbstractEntity {

    protected String login;
    protected String passwordHash;
    @XmlSchemaType(name = "string")
    protected RoleType roleType;

    /**
     * Gets the value of the login property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLogin(final String value) {
        this.login = value;
    }

    /**
     * Gets the value of the passwordHash property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Sets the value of the passwordHash property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPasswordHash(final String value) {
        this.passwordHash = value;
    }

    /**
     * Gets the value of the roleType property.
     *
     * @return
     *     possible object is
     *     {@link RoleType }
     *
     */
    public RoleType getRoleType() {
        return roleType;
    }

    /**
     * Sets the value of the roleType property.
     *
     * @param value
     *     allowed object is
     *     {@link RoleType }
     *
     */
    public void setRoleType(final RoleType value) {
        this.roleType = value;
    }

}
