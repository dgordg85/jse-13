
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userUpdatePassword complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="userUpdatePassword"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sessionBASE64" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="currentPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="newPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userUpdatePassword", propOrder = {
        "sessionBASE64",
        "currentPassword",
        "newPassword"
})
public class UserUpdatePassword {

    protected String sessionBASE64;
    protected String currentPassword;
    protected String newPassword;

    /**
     * Gets the value of the sessionBASE64 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSessionBASE64() {
        return sessionBASE64;
    }

    /**
     * Sets the value of the sessionBASE64 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSessionBASE64(final String value) {
        this.sessionBASE64 = value;
    }

    /**
     * Gets the value of the currentPassword property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCurrentPassword() {
        return currentPassword;
    }

    /**
     * Sets the value of the currentPassword property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCurrentPassword(final String value) {
        this.currentPassword = value;
    }

    /**
     * Gets the value of the newPassword property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Sets the value of the newPassword property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNewPassword(final String value) {
        this.newPassword = value;
    }

}
