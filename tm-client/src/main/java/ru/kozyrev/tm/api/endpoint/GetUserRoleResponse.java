
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for getUserRoleResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getUserRoleResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://endpoint.api.tm.kozyrev.ru/}roleType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserRoleResponse", propOrder = {
        "_return"
})
public class GetUserRoleResponse {

    @XmlElement(name = "return")
    @XmlSchemaType(name = "string")
    protected RoleType _return;

    /**
     * Gets the value of the return property.
     *
     * @return
     *     possible object is
     *     {@link RoleType }
     *
     */
    public RoleType getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value
     *     allowed object is
     *     {@link RoleType }
     *
     */
    public void setReturn(final RoleType value) {
        this._return = value;
    }

}
