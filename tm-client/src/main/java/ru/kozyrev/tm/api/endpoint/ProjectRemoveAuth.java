
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for projectRemoveAuth complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="projectRemoveAuth"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sessionBASE64" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="projectId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectRemoveAuth", propOrder = {
        "sessionBASE64",
        "projectId"
})
public class ProjectRemoveAuth {

    protected String sessionBASE64;
    protected String projectId;

    /**
     * Gets the value of the sessionBASE64 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSessionBASE64() {
        return sessionBASE64;
    }

    /**
     * Sets the value of the sessionBASE64 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSessionBASE64(final String value) {
        this.sessionBASE64 = value;
    }

    /**
     * Gets the value of the projectId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * Sets the value of the projectId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProjectId(final String value) {
        this.projectId = value;
    }

}
