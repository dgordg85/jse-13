
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for abstractObject complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="abstractObject"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.api.tm.kozyrev.ru/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateFinish" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="dateStart" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shortLink" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://endpoint.api.tm.kozyrev.ru/}documentStatus" minOccurs="0"/&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "abstractObject", propOrder = {
        "dateFinish",
        "dateStart",
        "description",
        "name",
        "shortLink",
        "status",
        "userId"
})
@XmlSeeAlso({
        Task.class,
        Project.class
})
public abstract class AbstractObject
        extends AbstractEntity {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateFinish;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateStart;
    protected String description;
    protected String name;
    protected Integer shortLink;
    @XmlSchemaType(name = "string")
    protected DocumentStatus status;
    protected String userId;

    /**
     * Gets the value of the dateFinish property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDateFinish() {
        return dateFinish;
    }

    /**
     * Sets the value of the dateFinish property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDateFinish(final XMLGregorianCalendar value) {
        this.dateFinish = value;
    }

    /**
     * Gets the value of the dateStart property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDateStart() {
        return dateStart;
    }

    /**
     * Sets the value of the dateStart property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDateStart(final XMLGregorianCalendar value) {
        this.dateStart = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescription(final String value) {
        this.description = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(final String value) {
        this.name = value;
    }

    /**
     * Gets the value of the shortLink property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getShortLink() {
        return shortLink;
    }

    /**
     * Sets the value of the shortLink property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setShortLink(final Integer value) {
        this.shortLink = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link DocumentStatus }
     *
     */
    public DocumentStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link DocumentStatus }
     *
     */
    public void setStatus(final DocumentStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the userId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserId(final String value) {
        this.userId = value;
    }

}
