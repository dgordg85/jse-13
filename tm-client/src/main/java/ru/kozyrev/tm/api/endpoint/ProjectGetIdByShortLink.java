
package ru.kozyrev.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for projectGetIdByShortLink complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="projectGetIdByShortLink"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sessionBASE64" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="num" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectGetIdByShortLink", propOrder = {
        "sessionBASE64",
        "num"
})
public class ProjectGetIdByShortLink {

    protected String sessionBASE64;
    protected String num;

    /**
     * Gets the value of the sessionBASE64 property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSessionBASE64() {
        return sessionBASE64;
    }

    /**
     * Sets the value of the sessionBASE64 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSessionBASE64(final String value) {
        this.sessionBASE64 = value;
    }

    /**
     * Gets the value of the num property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNum() {
        return num;
    }

    /**
     * Sets the value of the num property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNum(final String value) {
        this.num = value;
    }

}
