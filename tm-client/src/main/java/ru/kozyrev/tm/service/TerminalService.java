package ru.kozyrev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kozyrev.tm.api.endpoint.RoleType;
import ru.kozyrev.tm.api.service.EndpointLocator;
import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;
import ru.kozyrev.tm.exception.command.CommandCorruptException;
import ru.kozyrev.tm.exception.command.CommandException;

import java.util.*;

public final class TerminalService implements ITerminalService {
    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private StateService stateService;

    @NotNull
    private EndpointLocator endpointLocator;

    @NotNull
    private Scanner sc = new Scanner(System.in);

    @Override
    public final void printCommands() throws Exception {
        for (final AbstractCommand command : commands.values()) {
            if (isCommandAllow(command)) {
                System.out.println(command.getName() + ": " + command.getDescription());
            }
        }
    }

    @Override
    public final void registryCommand(@Nullable final AbstractCommand command) throws Exception {
        if (command == null) {
            throw new CommandCorruptException();
        }
        if (command.getName().isEmpty())
            throw new CommandCorruptException();
        if (command.getDescription().isEmpty())
            throw new CommandCorruptException();
        command.setEndpointLocator(endpointLocator);
        command.setStateService(stateService);
        command.setTerminalService(this);
        commands.put(command.getName().toLowerCase(), command);
    }

    @Override
    public final void execute(@Nullable final String commandStr) throws Exception {
        if (commandStr == null || commandStr.isEmpty()) {
            throw new CommandException();
        }
        @NotNull final AbstractCommand command = commands.get(commandStr.toLowerCase());
        if (!isCommandAllow(command)) {
            throw new AccessForbiddenException();
        }
        command.execute();
    }

    public final boolean isCommandAllow(@Nullable final AbstractCommand command) throws Exception {
        if (command == null) {
            throw new CommandException();
        }
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            return command.getSecure();
        }
        if (command.getSecure()) {
            return true;
        }
        @NotNull final List<RoleType> roleTypes = command.getRoleTypes();
        if (roleTypes.size() == 0) {
            throw new CommandException();
        }
        return roleTypes.contains(stateService.getRoleType());
    }

    @Override
    public final void initCommands() throws Exception {
        final Set<Class<? extends AbstractCommand>> classes2 =
                new Reflections("ru.kozyrev.tm").getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<?> clazz : classes2) {
            registryCommand((AbstractCommand) clazz.getDeclaredConstructor().newInstance());
        }
        sortCommand();
    }

    public final void sortCommand() {
        @NotNull final List<Map.Entry<String, AbstractCommand>> entries =
                new ArrayList<>(commands.entrySet());
        entries.sort(Comparator.comparingInt(a -> a.getValue().getSortId()));
        commands.clear();
        for (final Map.Entry<String, AbstractCommand> entry : entries) {
            commands.put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public final void welcomeMsg() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    @Override
    public final void errorDateMsg() {
        System.out.println("Wrong date! User dd-MM-YYYY format");
    }

    @NotNull
    @Override
    public final String nextLine() {
        return sc.nextLine();
    }

    @Override
    public final void closeSc() {
        sc.close();
    }

    @Override
    public final void setSc(@NotNull final Scanner sc) {
        this.sc = sc;
    }

    public void setEndpointLocator(@NotNull final EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }

    public void setStateService(@NotNull final StateService stateService) {
        this.stateService = stateService;
    }
}
