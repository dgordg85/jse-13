package ru.kozyrev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.enumerated.RoleType;

@Getter
@Setter
public final class StateService {
    @Nullable
    private String session = null;

    @Nullable
    private String login = null;

    @Nullable
    private ru.kozyrev.tm.api.endpoint.RoleType roleType = null;

    @NotNull
    private Column projectColumn = Column.NUM;

    @NotNull
    private Column taskColumn = Column.NUM;

    @NotNull
    private Direction projectDirection = Direction.ASC;

    @NotNull
    private Direction taskDirection = Direction.ASC;

    public final void clearSession() {
        session = null;
    }

    @NotNull
    public final String getRoleStr() {
        if (roleType == null) {
            return "";
        }
        return RoleType.valueOf(roleType.value()).getDisplayName();
    }
}
