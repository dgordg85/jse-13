package ru.kozyrev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.*;
import ru.kozyrev.tm.api.service.EndpointLocator;
import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.endpoint.*;
import ru.kozyrev.tm.exception.entity.DateException;
import ru.kozyrev.tm.service.StateService;
import ru.kozyrev.tm.service.TerminalService;

import java.lang.Exception;
import java.text.ParseException;

@Getter
public final class Bootstrap implements EndpointLocator {
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpointService().getAdminEndpointPort();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final StateService stateService = new StateService();

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    public final void init() throws Exception {
        getTerminalService().welcomeMsg();
        terminalService.setStateService(stateService);
        terminalService.setEndpointLocator(this);
        getTerminalService().initCommands();
        @Nullable String command;
        while (true) {
            try {
                command = terminalService.nextLine().toLowerCase();
                getTerminalService().execute(command);
            } catch (final ParseException | DateException e) {
                getTerminalService().errorDateMsg();
            } catch (final Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
