package ru.kozyrev.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.User;
import ru.kozyrev.tm.util.HashUtil;

import javax.xml.ws.WebServiceException;

import static org.junit.jupiter.api.Assertions.assertThrows;

class SessionEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void openSession() throws Throwable {
        @NotNull final User user = new User();
        @NotNull final String password = HashUtil.getHash("TEST");
        user.setLogin("TEST");
        user.setPasswordHash(password);

        context.userEndpoint.userPersist(user);
        context.sessionEndpoint.openSession("TEST", password);
    }

    @Test
    void closeSession() throws Throwable {
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        context.sessionEndpoint.closeSession(context.getSession());

        @NotNull final WebServiceException exception = assertThrows(WebServiceException.class,
                () -> context.adminEndpoint.projectFindAll(context.session)
        );
        Assert.assertTrue(exception.getMessage().contains("Session not valid!"));
    }

}