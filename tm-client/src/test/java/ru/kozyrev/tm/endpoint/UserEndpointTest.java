package ru.kozyrev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.User;
import ru.kozyrev.tm.util.HashUtil;

import java.util.List;


class UserEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void userPersist() throws Throwable {
        context.userEndpoint.userPersist(context.user1);
        @Nullable final List<User> list = context.adminEndpoint.userFindAll(context.session);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());
        Assert.assertNotNull(context.userEndpoint.userGetUserByLogin(context.session, context.user1.getLogin()));
        Assert.assertNotNull(context.userEndpoint.userFindOne(context.session, context.user1.getId()));

    }

    @Test
    void userGetUserByLogin() throws Throwable {
        Assert.assertNotNull(context.userEndpoint.userGetUserByLogin(context.session, "user"));
        Assert.assertNotNull(context.userEndpoint.userGetUserByLogin(context.session, "admin"));
    }

    @Test
    void userUpdatePassword() throws Throwable {
        @NotNull final String oldPass = "TEST";
        @NotNull final String newPass = "BRHR";
        context.createTestUserAddDbCreateSession(oldPass);
        context.userEndpoint.userUpdatePassword(context.session, HashUtil.getHash(oldPass), newPass);
        Assert.assertNotNull(context.sessionEndpoint.openSession(oldPass, newPass));
    }

    @Test
    void userRemove() throws Throwable {
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());
        @NotNull final User user = context.userEndpoint.userGetUserByLogin(context.session, "user");
        context.userEndpoint.userRemove(context.session, user.getId());
        Assert.assertEquals(1, context.adminEndpoint.userFindAll(context.session).size());
    }

    @Test
    void userFindOne() throws Throwable {
        context.persistUserCreateSession(context.user3);
        Assert.assertNotNull(context.userEndpoint.userFindOne(context.session, context.user3.getId()));
    }

    @Test
    void userGetYourself() throws Throwable {
        context.persistUserCreateSession(context.user2);
        @NotNull final User userDb = context.userEndpoint.userGetYourself(context.session);
        Assert.assertEquals(context.user2.getId(), userDb.getId());
        Assert.assertEquals(context.user2.getLogin(), userDb.getLogin());
        Assert.assertEquals(context.user2.getRoleType(), userDb.getRoleType());
    }

    @Test
    void userMergeLogin() throws Throwable {
        context.persistUserCreateSession(context.user1);
        @NotNull final User user = context.userEndpoint.userGetYourself(context.session);
        user.setLogin("User55");
        user.setPasswordHash("");
        context.userEndpoint.userMerge(context.session, user);
        Assert.assertNotNull(context.sessionEndpoint.openSession("User55", context.user1.getPasswordHash()));
    }

    @Test
    void userMergePass() throws Throwable {
        context.persistUserCreateSession(context.user1);
        @NotNull final User user = context.userEndpoint.userGetYourself(context.session);
        user.setPasswordHash(HashUtil.getHash("User55"));
        context.userEndpoint.userMerge(context.session, user);
        Assert.assertNotNull(context.sessionEndpoint.openSession(
                context.user1.getLogin(), HashUtil.getHash("User55")));
    }

    @Test
    final void userRemoveByUserId() throws Throwable {
        context.persistUserCreateSession(context.user2);
        context.createSessionAdmin();
        context.userEndpoint.userRemove(context.session, context.user2.getId());
        Assert.assertNull(context.userEndpoint.userGetUserByLogin(context.session, context.user2.getLogin()));
    }
}
