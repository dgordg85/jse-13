package ru.kozyrev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.DocumentStatus;
import ru.kozyrev.tm.api.endpoint.Project;
import ru.kozyrev.tm.util.DateUtil;

import java.util.List;

class ProjectEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void projectFindOneAuth() throws Throwable {
        Assert.assertNotNull(context.projectEndpoint.projectFindOneAuth(context.session, context.project1Id));
        Assert.assertNotNull(context.projectEndpoint.projectFindOneAuth(context.session, context.project2Id));
        Assert.assertNull(context.projectEndpoint.projectFindOneAuth(context.session, context.project3Id));
    }

    @Test
    void projectRemoveAuth() throws Throwable {
        Assert.assertEquals(2, context.projectEndpoint.projectFindAllAuth(context.session).size());
        context.projectEndpoint.projectRemoveAuth(context.session, context.project1Id);
        Assert.assertNull(context.projectEndpoint.projectFindOneAuth(context.session, context.project1Id));
        Assert.assertEquals(1, context.projectEndpoint.projectFindAllAuth(context.session).size());
    }

    @Test
    void projectRemoveByShortLink() throws Throwable {
        Assert.assertEquals(2, context.projectEndpoint.projectFindAllAuth(context.session).size());
        context.projectEndpoint.projectRemoveByShortLink(context.session, "2");
        Assert.assertEquals(1, context.projectEndpoint.projectFindAllAuth(context.session).size());
    }

    @Test
    void projectPersistAuth() throws Throwable {
        @NotNull final Project project = new Project();
        project.setName("Новое имя проекта");
        project.setDescription("Описание проекта изменено");
        project.setDateStart(DateUtil.parseDateXML("01-01-1999"));
        project.setDateFinish(DateUtil.parseDateXML("01-01-2001"));
        project.setStatus(DocumentStatus.IN_PROGRESS);
        context.projectEndpoint.projectPersistAuth(context.session, project);

        @Nullable final String projectId =
                context.projectEndpoint.projectGetIdByShortLink(context.session, "4");
        @NotNull final Project projectDB = context.projectEndpoint.projectFindOneAuth(context.session, projectId);

        Assert.assertEquals("Новое имя проекта", projectDB.getName());
        Assert.assertEquals("Описание проекта изменено", projectDB.getDescription());
        Assert.assertEquals(DateUtil.parseDateXML("01-01-1999"), projectDB.getDateStart());
        Assert.assertEquals(DateUtil.parseDateXML("01-01-2001"), projectDB.getDateFinish());
        Assert.assertEquals(DocumentStatus.IN_PROGRESS, projectDB.getStatus());

    }

    @Test
    void projectRemoveAllAuth() throws Throwable {
        context.projectEndpoint.projectRemoveAllAuth(context.session);
        Assert.assertEquals(0, context.projectEndpoint.projectFindAllAuth(context.session).size());
    }

    @Test
    void projectFindWord() throws Throwable {
        Assert.assertEquals(2, context.projectEndpoint.projectFindWord(context.session, "проект").size());
    }

    @Test
    void projectFindAllAuth() throws Throwable {
        Assert.assertEquals(2, context.projectEndpoint.projectFindAllAuth(context.session).size());
    }

    @Test
    void projectGetIdByShortLink() throws Throwable {
        context.userEndpoint.userPersist(context.user1);
        context.projectEndpoint.projectPersistAuth(context.session, context.project1);
        context.taskEndpoint.taskPersistAuth(context.session, context.task1);

        @Nullable final String projectId = context.projectEndpoint.projectGetIdByShortLink(context.session, "4");
        Assert.assertEquals(context.project1.getId(), projectId);
    }

    @Test
    void projectMergeAuth() throws Throwable {
        @Nullable final Project project = context.projectEndpoint.projectFindOneAuth(context.session, context.project2Id);
        project.setName("Новое имя проекта");
        project.setDescription("Описание проекта изменено");
        project.setDateStart(DateUtil.parseDateXML("01-01-1999"));
        project.setDateFinish(DateUtil.parseDateXML("01-01-2001"));
        project.setStatus(DocumentStatus.IN_PROGRESS);
        context.projectEndpoint.projectMergeAuth(context.session, project);

        @Nullable final Project projectDB = context.projectEndpoint.projectFindOneAuth(context.session, context.project2Id);
        Assert.assertEquals("Новое имя проекта", projectDB.getName());
        Assert.assertEquals("Описание проекта изменено", projectDB.getDescription());
        Assert.assertEquals(DateUtil.parseDateXML("01-01-1999"), projectDB.getDateStart());
        Assert.assertEquals(DateUtil.parseDateXML("01-01-2001"), projectDB.getDateFinish());
        Assert.assertEquals(DocumentStatus.IN_PROGRESS, projectDB.getStatus());
    }

    @Test
    void projectFindAllBySortingNumAsc() throws Throwable {
        @NotNull final List<Project> list =
                context.projectEndpoint.projectFindAllBySorting(context.session, Column.NUM, Direction.ASC);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        Assert.assertEquals(1, (int) list.get(0).getShortLink());
        Assert.assertEquals(2, (int) list.get(1).getShortLink());
    }

    @Test
    void projectFindAllBySortingNumDesc() throws Throwable {
        @NotNull final List<Project> list =
                context.projectEndpoint.projectFindAllBySorting(context.session, Column.NUM, Direction.DESC);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        Assert.assertEquals(2, (int) list.get(0).getShortLink());
        Assert.assertEquals(1, (int) list.get(1).getShortLink());
    }

    @Test
    void projectFindAllBySortingStartAsc() throws Throwable {
        @NotNull final List<Project> list =
                context.projectEndpoint.projectFindAllBySorting(context.session, Column.DATE_START, Direction.ASC);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        Assert.assertEquals(2, (int) list.get(0).getShortLink());
        Assert.assertEquals(1, (int) list.get(1).getShortLink());
    }

    @Test
    void projectFindAllBySortingStartDesc() throws Throwable {
        @NotNull final List<Project> list =
                context.projectEndpoint.projectFindAllBySorting(context.session, Column.DATE_START, Direction.DESC);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        Assert.assertEquals(1, (int) list.get(0).getShortLink());
        Assert.assertEquals(2, (int) list.get(1).getShortLink());
    }

    @Test
    void projectFindAllBySortingFinishAsc() throws Throwable {
        @NotNull final List<Project> list =
                context.projectEndpoint.projectFindAllBySorting(context.session, Column.DATE_FINISH, Direction.ASC);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        Assert.assertEquals(1, (int) list.get(0).getShortLink());
        Assert.assertEquals(2, (int) list.get(1).getShortLink());
    }

    @Test
    void projectFindAllBySortingFinishDesc() throws Throwable {
        @NotNull final List<Project> list =
                context.projectEndpoint.projectFindAllBySorting(context.session, Column.DATE_FINISH, Direction.DESC);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        Assert.assertEquals(2, (int) list.get(0).getShortLink());
        Assert.assertEquals(1, (int) list.get(1).getShortLink());
    }

    @Test
    void projectFindAllBySortingStatusAsc() throws Throwable {
        @NotNull final List<Project> list =
                context.projectEndpoint.projectFindAllBySorting(context.session, Column.STATUS, Direction.ASC);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        Assert.assertEquals(2, (int) list.get(0).getShortLink());
        Assert.assertEquals(1, (int) list.get(1).getShortLink());
    }

    @Test
    void projectFindAllBySortingStatusDesc() throws Throwable {
        @NotNull final List<Project> list =
                context.projectEndpoint.projectFindAllBySorting(context.session, Column.STATUS, Direction.DESC);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());

        Assert.assertEquals(1, (int) list.get(0).getShortLink());
        Assert.assertEquals(2, (int) list.get(1).getShortLink());
    }
}
