package ru.kozyrev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.Project;
import ru.kozyrev.tm.api.endpoint.Task;
import ru.kozyrev.tm.api.endpoint.User;

import java.util.ArrayList;
import java.util.List;

class AdminEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void adminXmlSaveFasterXML() throws Throwable {
        context.adminEndpoint.adminXmlSaveFasterXML(context.session);

        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());

        context.adminEndpoint.projectRemoveAll(context.session);
        context.adminEndpoint.userRemoveAll(context.session);
        Assert.assertEquals(0, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(0, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(1, context.adminEndpoint.userFindAll(context.session).size());

        context.adminEndpoint.adminXmlLoadFasterXML(context.session);

        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());
    }

    @Test
    void adminXmlSaveLoadJaxB() throws Throwable {
        context.adminEndpoint.adminXmlSaveJaxB(context.session);

        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());

        context.adminEndpoint.projectRemoveAll(context.session);
        context.adminEndpoint.userRemoveAll(context.session);
        Assert.assertEquals(0, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(0, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(1, context.adminEndpoint.userFindAll(context.session).size());

        context.adminEndpoint.adminXmlLoadJaxB(context.session);

        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());
    }

    @Test
    void adminBinSaveLoad() throws Throwable {
        context.adminEndpoint.adminBinSave(context.session);

        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());

        context.adminEndpoint.projectRemoveAll(context.session);
        context.adminEndpoint.userRemoveAll(context.session);
        Assert.assertEquals(0, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(0, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(1, context.adminEndpoint.userFindAll(context.session).size());

        context.adminEndpoint.adminBinLoad(context.session);

        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());
    }

    @Test
    void userRemoveAll() throws Throwable {
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());
        context.adminEndpoint.userRemoveAll(context.session);
        Assert.assertEquals(1, context.adminEndpoint.userFindAll(context.session).size());
    }

    @Test
    void adminJsonSaveLoadFasterXML() throws Throwable {
        context.adminEndpoint.adminJsonSaveFasterXML(context.session);
        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());

        context.adminEndpoint.projectRemoveAll(context.session);
        context.adminEndpoint.userRemoveAll(context.session);
        Assert.assertEquals(0, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(0, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(1, context.adminEndpoint.userFindAll(context.session).size());

        context.adminEndpoint.adminJsonLoadFasterXML(context.session);

        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());
    }

    @Test
    void adminJsonSaveLoadJaxB() throws Throwable {
        context.adminEndpoint.adminJsonSaveJaxB(context.session);

        context.adminEndpoint.adminJsonLoadJaxB(context.session);
    }

    @Test
    void taskRemoveAll() throws Throwable {
        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
        context.adminEndpoint.taskRemoveAll(context.session);
        Assert.assertEquals(0, context.adminEndpoint.taskFindAll(context.session).size());
    }

    @Test
    void taskFindAll() throws Throwable {
        Assert.assertEquals(9, context.adminEndpoint.taskFindAll(context.session).size());
    }

    @Test
    void projectRemoveAll() throws Throwable {
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
        context.adminEndpoint.projectRemoveAll(context.session);
        Assert.assertEquals(0, context.adminEndpoint.projectFindAll(context.session).size());
    }

    @Test
    void projectFindAll() throws Throwable {
        Assert.assertEquals(3, context.adminEndpoint.projectFindAll(context.session).size());
    }

    @Test
    void userPersistList() throws Throwable {
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());
        @NotNull final List<User> list = new ArrayList<>();
        list.add(context.user1);
        list.add(context.user2);
        list.add(context.user3);
        context.adminEndpoint.userPersistList(context.session, list);
        Assert.assertEquals(5, context.adminEndpoint.userFindAll(context.session).size());
    }

    @Test
    void taskPersistList() throws Throwable {
        projectPersistList();
        @NotNull final List<Task> list = new ArrayList<>();
        list.add(context.task1);
        list.add(context.task2);
        list.add(context.task3);
        context.adminEndpoint.taskPersistList(context.session, list);
        Assert.assertEquals(12, context.adminEndpoint.taskFindAll(context.session).size());
    }

    @Test
    void projectPersistList() throws Throwable {
        userPersistList();
        @NotNull final List<Project> list = new ArrayList<>();
        list.add(context.project1);
        list.add(context.project2);
        list.add(context.project3);
        context.adminEndpoint.projectPersistList(context.session, list);
        Assert.assertEquals(6, context.adminEndpoint.projectFindAll(context.session).size());
    }

    @Test
    void userFindAll() throws Throwable {
        Assert.assertEquals(2, context.adminEndpoint.userFindAll(context.session).size());
    }
}
