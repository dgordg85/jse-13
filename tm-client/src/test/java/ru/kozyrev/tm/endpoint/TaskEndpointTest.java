package ru.kozyrev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.kozyrev.tm.api.endpoint.Column;
import ru.kozyrev.tm.api.endpoint.Direction;
import ru.kozyrev.tm.api.endpoint.Task;
import ru.kozyrev.tm.util.DateUtil;

import java.util.List;

class TaskEndpointTest {
    @NotNull
    private Context context;

    @BeforeEach
    void setUp() throws Throwable {
        context = new Context();
        context.setUp();
    }

    @AfterEach
    void tearDown() throws Throwable {
        context.tearDown();
    }

    @Test
    void taskFindAllByProjectAuthSortNumAsc() throws Throwable {
        @NotNull final List<Task> list =
                context.taskEndpoint.taskFindAllByProjectAuthSort(context.session, context.project1Id, Column.NUM, Direction.ASC);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());

        Assert.assertEquals(1, (int) list.get(0).getShortLink());
        Assert.assertEquals(2, (int) list.get(1).getShortLink());
        Assert.assertEquals(3, (int) list.get(2).getShortLink());
    }

    @Test
    void taskFindAllByProjectAuthSortNumDesc() throws Throwable {
        @NotNull final List<Task> list =
                context.taskEndpoint.taskFindAllByProjectAuthSort(context.session, context.project1Id, Column.NUM, Direction.DESC);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());

        Assert.assertEquals(3, (int) list.get(0).getShortLink());
        Assert.assertEquals(2, (int) list.get(1).getShortLink());
        Assert.assertEquals(1, (int) list.get(2).getShortLink());
    }

    @Test
    void taskFindAllByProjectAuthSortStartAsc() throws Throwable {
        @NotNull final List<Task> list =
                context.taskEndpoint.taskFindAllByProjectAuthSort(context.session, context.project1Id, Column.DATE_START, Direction.ASC);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());

        Assert.assertEquals(1, (int) list.get(0).getShortLink());
        Assert.assertEquals(2, (int) list.get(1).getShortLink());
        Assert.assertEquals(3, (int) list.get(2).getShortLink());
    }

    @Test
    void taskFindAllByProjectAuthSortStartDesc() throws Throwable {
        @NotNull final List<Task> list =
                context.taskEndpoint.taskFindAllByProjectAuthSort(context.session, context.project1Id, Column.DATE_START, Direction.DESC);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());

        Assert.assertEquals(3, (int) list.get(0).getShortLink());
        Assert.assertEquals(2, (int) list.get(1).getShortLink());
        Assert.assertEquals(1, (int) list.get(2).getShortLink());
    }

    @Test
    void taskFindAllByProjectAuthSortFinishAsc() throws Throwable {
        @NotNull final List<Task> list =
                context.taskEndpoint.taskFindAllByProjectAuthSort(context.session, context.project1Id, Column.DATE_FINISH, Direction.ASC);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());

        Assert.assertEquals(2, (int) list.get(0).getShortLink());
        Assert.assertEquals(3, (int) list.get(1).getShortLink());
        Assert.assertEquals(1, (int) list.get(2).getShortLink());
    }

    @Test
    void taskFindAllByProjectAuthSortFinishDesc() throws Throwable {
        @NotNull final List<Task> list =
                context.taskEndpoint.taskFindAllByProjectAuthSort(context.session, context.project1Id, Column.DATE_FINISH, Direction.DESC);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());

        Assert.assertEquals(1, (int) list.get(0).getShortLink());
        Assert.assertEquals(3, (int) list.get(1).getShortLink());
        Assert.assertEquals(2, (int) list.get(2).getShortLink());
    }

    @Test
    void taskFindAllByProjectAuthSortStatusAsc() throws Throwable {
        @NotNull final List<Task> list =
                context.taskEndpoint.taskFindAllByProjectAuthSort(context.session, context.project1Id, Column.STATUS, Direction.ASC);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());

        Assert.assertEquals(2, (int) list.get(0).getShortLink());
        Assert.assertEquals(3, (int) list.get(1).getShortLink());
        Assert.assertEquals(1, (int) list.get(2).getShortLink());
    }

    @Test
    void taskFindAllByProjectAuthSortStatusDesc() throws Throwable {
        @NotNull final List<Task> list =
                context.taskEndpoint.taskFindAllByProjectAuthSort(context.session, context.project1Id, Column.STATUS, Direction.DESC);
        Assert.assertNotNull(list);
        Assert.assertEquals(3, list.size());

        Assert.assertEquals(1, (int) list.get(0).getShortLink());
        Assert.assertEquals(3, (int) list.get(1).getShortLink());
        Assert.assertEquals(2, (int) list.get(2).getShortLink());
    }

    @Test
    void taskGetIdByShortLink() throws Throwable {
        Assert.assertNotNull(context.taskEndpoint.taskGetIdByShortLink(context.session, "8"));
    }

    @Test
    void taskPersistAuth() throws Throwable {
        context.userEndpoint.userPersist(context.user1);
        context.projectEndpoint.projectPersistAuth(context.session, context.project1);
        context.taskEndpoint.taskPersistAuth(context.session, context.task1);
        Assert.assertNotNull(context.taskEndpoint.taskFindOneAuth(context.session, context.task1.getId()));
    }

    @Test
    void taskRemoveAllByProjectAuth() throws Throwable {
        Assert.assertEquals(3, context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project1Id).size());
        context.taskEndpoint.taskRemoveAllByProjectAuth(context.session, context.project1Id);
        Assert.assertEquals(0, context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project1Id).size());

        Assert.assertEquals(5, context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project2Id).size());
    }

    @Test
    void taskRemoveAllByProjectNumAuth() throws Throwable {
        Assert.assertEquals(5, context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project2Id).size());
        context.taskEndpoint.taskRemoveAllByProjectNumAuth(context.session, "2");
        Assert.assertEquals(0, context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project2Id).size());

        Assert.assertEquals(3, context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project1Id).size());
    }

    @Test
    void taskFindAllAuth() throws Throwable {
        Assert.assertEquals(8, context.taskEndpoint.taskFindAllAuth(context.session).size());
    }

    @Test
    void taskRemoveAuth() throws Throwable {
        Assert.assertEquals(8, context.taskEndpoint.taskFindAllAuth(context.session).size());
        context.taskEndpoint.taskRemoveAuth(context.session, context.task7Id);
        Assert.assertEquals(7, context.taskEndpoint.taskFindAllAuth(context.session).size());
        Assert.assertEquals(4, context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project2Id).size());
    }

    @Test
    void taskMergeAuth() throws Throwable {
        @Nullable final Task testTask = context.taskEndpoint.taskFindOneAuth(context.session, context.task5Id);
        Assert.assertNotNull(testTask);
        testTask.setName("Новое имя");
        testTask.setDescription("Новое описание");
        testTask.setDateStart(DateUtil.parseDateXML("12-12-2013"));
        testTask.setDateFinish(DateUtil.parseDateXML("01-01-2013"));
        context.taskEndpoint.taskMergeAuth(context.session, testTask);
        @Nullable final Task taskDB = context.taskEndpoint.taskFindOneAuth(context.session, context.task5Id);
        Assert.assertEquals("Новое имя", taskDB.getName());
        Assert.assertEquals("Новое описание", taskDB.getDescription());
        Assert.assertEquals(DateUtil.parseDateXML("12-12-2013"), taskDB.getDateStart());
        Assert.assertEquals(DateUtil.parseDateXML("01-01-2013"), taskDB.getDateFinish());
    }

    @Test
    void taskRemoveAllAuth() throws Throwable {
        context.taskEndpoint.taskRemoveAllAuth(context.session);
        Assert.assertEquals(0, context.taskEndpoint.taskFindAllAuth(context.session).size());
    }

    @Test
    void taskFindWord() throws Throwable {
        Assert.assertEquals(3, context.taskEndpoint.taskFindWord(context.session, "дан").size());
    }

    @Test
    void taskFindOneAuth() throws Throwable {
        Assert.assertNotNull(context.taskEndpoint.taskFindOneAuth(context.session, context.task4Id));
    }

    @Test
    void taskFindAllByProjectAuth() throws Throwable {
        Assert.assertEquals(5,
                context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project2Id).size());
    }

    @Test
    void taskRemoveByShortLink() throws Throwable {
        Assert.assertEquals(3,
                context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project1Id).size());
        context.taskEndpoint.taskRemoveByShortLink(context.session, "1");
        context.taskEndpoint.taskRemoveByShortLink(context.session, "2");
        context.taskEndpoint.taskRemoveByShortLink(context.session, "3");
        Assert.assertEquals(0,
                context.taskEndpoint.taskFindAllByProjectAuth(context.session, context.project1Id).size());
    }
}