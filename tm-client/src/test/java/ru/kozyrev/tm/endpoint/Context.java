package ru.kozyrev.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.endpoint.*;
import ru.kozyrev.tm.context.Bootstrap;
import ru.kozyrev.tm.util.DateUtil;
import ru.kozyrev.tm.util.HashUtil;

@Getter
@Setter
public class Context {
    @NotNull
    public final Bootstrap bootstrap;
    @NotNull
    public final IAdminEndpoint adminEndpoint;
    @NotNull
    public final IProjectEndpoint projectEndpoint;
    @NotNull
    public final ISessionEndpoint sessionEndpoint;
    @NotNull
    public final ITaskEndpoint taskEndpoint;
    @NotNull
    public final IUserEndpoint userEndpoint;
    @Nullable
    public String session = null;
    @NotNull Project project1;
    @NotNull Project project2;
    @NotNull Project project3;
    @NotNull Task task1;
    @NotNull Task task2;
    @NotNull Task task3;
    @NotNull String project1Id;
    @NotNull String project2Id;
    @NotNull String project3Id;
    @NotNull String task1Id;
    @NotNull String task2Id;
    @NotNull String task3Id;
    @NotNull String task4Id;
    @NotNull String task5Id;
    @NotNull String task6Id;
    @NotNull String task7Id;
    @NotNull String task8Id;
    @NotNull String task9Id;
    @NotNull String task10Id;
    @NotNull String task11Id;
    @NotNull String task12Id;
    @NotNull User user1;
    @NotNull User user2;
    @NotNull User user3;


    public Context() {
        bootstrap = new Bootstrap();
        adminEndpoint = bootstrap.getAdminEndpoint();
        projectEndpoint = bootstrap.getProjectEndpoint();
        sessionEndpoint = bootstrap.getSessionEndpoint();
        taskEndpoint = bootstrap.getTaskEndpoint();
        userEndpoint = bootstrap.getUserEndpoint();
    }

    void setUp() throws Throwable {
        tearDown();

        createUsers();
        createProjects();
        createTasks();

        createSessionAdmin();
        createUser();
        fillList();
    }

    void tearDown() throws Throwable {
        createSessionAdmin();
        adminEndpoint.projectRemoveAll(session);
        adminEndpoint.taskRemoveAll(session);
        adminEndpoint.userRemoveAll(session);
        sessionEndpoint.closeSession(session);
        session = null;
    }

    public final void createUser() throws Throwable {
        @Nullable final User user = new User();
        user.setLogin("user");
        user.setPasswordHash(HashUtil.getHash("user"));
        user.setRoleType(RoleType.USER);
        userEndpoint.userPersist(user);
    }

    public void createSessionAdmin() throws Throwable {
        @NotNull final String passwordHash = HashUtil.getHash("admin");
        session = sessionEndpoint.openSession("admin", passwordHash);
    }

    public void createSessionUser() throws Throwable {
        @NotNull final String passwordHash = HashUtil.getHash("user");
        session = sessionEndpoint.openSession("user", passwordHash);
    }

    public void createTestUserAddDbCreateSession(@NotNull final String loginPass) throws Throwable {
        @NotNull final String login = loginPass;
        @NotNull final String passwordHash = HashUtil.getHash(loginPass);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @Nullable final User userReturn = userEndpoint.userPersist(user);
        session = sessionEndpoint.openSession(login, passwordHash);
    }

    public void persistUserCreateSession(@NotNull final User user) throws Throwable {
        @NotNull final String login = user.getLogin();
        @NotNull final String passwordHash = user.getPasswordHash();
        userEndpoint.userPersist(user);
        session = sessionEndpoint.openSession(login, passwordHash);
    }

    public void fillList() throws Throwable {
        createSessionAdmin();
        @NotNull final Project project1 = new Project();
        project1.setName("Проект 1");
        project1.setDescription("Удаление данных");
        project1.setDateStart(DateUtil.parseDateXML("30-04-2020"));
        project1.setDateFinish(DateUtil.parseDateXML("09-04-2020"));
        project1.setStatus(DocumentStatus.READY);

        @NotNull final Project project2 = new Project();
        project2.setName("Проект 2");
        project2.setDescription("Восстановление");
        project2.setDateStart(DateUtil.parseDateXML("10-04-2020"));
        project2.setDateFinish(DateUtil.parseDateXML("05-05-2020"));
        project1.setStatus(DocumentStatus.IN_PROGRESS);

        projectEndpoint.projectPersistAuth(session, project1);
        projectEndpoint.projectPersistAuth(session, project2);

        project1Id = projectEndpoint.projectGetIdByShortLink(session, "1");
        project2Id = projectEndpoint.projectGetIdByShortLink(session, "2");

        @NotNull final Task task1 = new Task();
        task1.setName("Задача 1");
        task1.setProjectId(project1Id);
        task1.setDescription("Получить доступы");
        task1.setDateStart(DateUtil.parseDateXML("01-04-2020"));
        task1.setDateFinish(DateUtil.parseDateXML("25-04-2020"));
        task1.setStatus(DocumentStatus.READY);
        taskEndpoint.taskPersistAuth(session, task1);
        task1Id = taskEndpoint.taskGetIdByShortLink(session, "1");

        @NotNull final Task task2 = new Task();
        task2.setName("Задача 2");
        task2.setProjectId(project1Id);
        task2.setDescription("Удалить данные");
        task2.setDateStart(DateUtil.parseDateXML("05-04-2020"));
        task2.setDateFinish(DateUtil.parseDateXML("06-04-2020"));
        task2.setStatus(DocumentStatus.PLANNED);
        taskEndpoint.taskPersistAuth(session, task2);
        task2Id = taskEndpoint.taskGetIdByShortLink(session, "2");

        @NotNull final Task task3 = new Task();
        task3.setName("Задача 3");
        task3.setProjectId(project1Id);
        task3.setDescription("Восстановить данные");
        task3.setDateStart(DateUtil.parseDateXML("07-04-2020"));
        task3.setDateFinish(DateUtil.parseDateXML("09-04-2020"));
        task3.setStatus(DocumentStatus.IN_PROGRESS);
        taskEndpoint.taskPersistAuth(session, task3);
        task3Id = taskEndpoint.taskGetIdByShortLink(session, "3");

        @NotNull final Task task4 = new Task();
        task4.setName("Задача 1");
        task4.setProjectId(project2Id);
        task4.setDescription("Получить вводные");
        task4.setDateStart(DateUtil.parseDateXML("10-04-2020"));
        task4.setDateFinish(DateUtil.parseDateXML("11-04-2020"));
        task4.setStatus(DocumentStatus.READY);
        taskEndpoint.taskPersistAuth(session, task4);
        task4Id = taskEndpoint.taskGetIdByShortLink(session, "4");

        @NotNull final Task task5 = new Task();
        task5.setName("Задача 2");
        task5.setProjectId(project2Id);
        task5.setDescription("Сделать бекап");
        task5.setDateStart(DateUtil.parseDateXML("12-04-2020"));
        task5.setDateFinish(DateUtil.parseDateXML("12-04-2020"));
        task5.setStatus(DocumentStatus.READY);
        taskEndpoint.taskPersistAuth(session, task5);
        task5Id = taskEndpoint.taskGetIdByShortLink(session, "5");

        @NotNull final Task task6 = new Task();
        task6.setName("Задача 3");
        task6.setProjectId(project2Id);
        task6.setDescription("Проверить данные");
        task6.setDateStart(DateUtil.parseDateXML("13-04-2020"));
        task6.setDateFinish(DateUtil.parseDateXML("15-04-2020"));
        task6.setStatus(DocumentStatus.PLANNED);
        taskEndpoint.taskPersistAuth(session, task6);
        task6Id = taskEndpoint.taskGetIdByShortLink(session, "6");

        @NotNull final Task task7 = new Task();
        task7.setName("Задача 4");
        task7.setProjectId(project2Id);
        task7.setDescription("Попытаться восстановить");
        task7.setDateStart(DateUtil.parseDateXML("16-04-2020"));
        task7.setDateFinish(DateUtil.parseDateXML("27-04-2020"));
        task7.setStatus(DocumentStatus.READY);
        taskEndpoint.taskPersistAuth(session, task7);
        task7Id = taskEndpoint.taskGetIdByShortLink(session, "7");

        @NotNull final Task task8 = new Task();
        task8.setName("Задача 5");
        task8.setProjectId(project2Id);
        task8.setDescription("Передать клиенту");
        task8.setDateStart(DateUtil.parseDateXML("28-04-2020"));
        task8.setDateFinish(DateUtil.parseDateXML("05-05-2020"));
        task8.setStatus(DocumentStatus.PLANNED);
        taskEndpoint.taskPersistAuth(session, task8);
        task8Id = taskEndpoint.taskGetIdByShortLink(session, "8");

        createSessionUser();

        @NotNull final Project project3 = new Project();
        project3.setName("Проект 3");
        project3.setDescription("Просто проект");
        project3.setDateStart(DateUtil.parseDateXML("22-02-2022"));
        project3.setDateFinish(DateUtil.parseDateXML("23-03-2023"));
        projectEndpoint.projectPersistAuth(session, project3);

        @NotNull final Task task9 = new Task();
        task9.setName("Задача 1");
        project3Id = projectEndpoint.projectGetIdByShortLink(session, "3");
        task9.setProjectId(project3Id);
        task9.setDescription("Просто таск");
        task9.setDateStart(DateUtil.parseDateXML("01-04-2020"));
        task9.setDateFinish(DateUtil.parseDateXML("04-04-2020"));
        task9.setStatus(DocumentStatus.IN_PROGRESS);
        taskEndpoint.taskPersistAuth(session, task9);
        task1Id = taskEndpoint.taskGetIdByShortLink(session, "9");

        createSessionAdmin();
    }

    void createUsers() {
        @NotNull final String loginPass1 = "user1";
        user1 = new User();
        user1.setLogin(loginPass1);
        user1.setPasswordHash(HashUtil.getHash(loginPass1));
        user1.setRoleType(RoleType.USER);
        user1.setId("98ebe1bf-eb74-4db5-a835-dd7f47a1e21b");

        @NotNull final String loginPass2 = "user2";
        user2 = new User();
        user2.setLogin(loginPass2);
        user2.setPasswordHash(HashUtil.getHash(loginPass2));
        user2.setRoleType(RoleType.USER);
        user2.setId("4ea9da55-52f1-47e7-9c5e-ee44a0a95819");

        @NotNull final String loginPass3 = "user3";
        user3 = new User();
        user3.setLogin(loginPass3);
        user3.setPasswordHash(HashUtil.getHash(loginPass3));
        user3.setRoleType(RoleType.ADMIN);
        user3.setId("7f4ae477-2293-4026-b95a-0ce9de07c016");
    }

    void createProjects() throws java.lang.Exception {
        project1 = new Project();
        project1.setName("Тестовый проект 1");
        project1.setDescription("Тестовое описание 2");
        project1.setDateStart(DateUtil.parseDateXML("09-09-99"));
        project1.setDateFinish(DateUtil.parseDateXML("01-01-01"));
        project1.setStatus(DocumentStatus.IN_PROGRESS);
        project1.setId("5152afff-6c04-4587-8df3-36e5f40779f0");
        project1.setUserId("98ebe1bf-eb74-4db5-a835-dd7f47a1e21b");

        project2 = new Project();
        project2.setName("Тестовый проект 2");
        project2.setDescription("Тестовое описание 2");
        project2.setDateStart(DateUtil.parseDateXML("08-08-88"));
        project2.setDateFinish(DateUtil.parseDateXML("12-12-12"));
        project2.setStatus(DocumentStatus.READY);
        project2.setId("c0a2f36e-95dc-4812-98af-4013e30f9e1f");
        project2.setUserId("4ea9da55-52f1-47e7-9c5e-ee44a0a95819");

        project3 = new Project();
        project3.setName("Тестовый проект 3");
        project3.setDescription("Тестовое описание 3");
        project3.setDateStart(DateUtil.parseDateXML("07-07-77"));
        project3.setDateFinish(DateUtil.parseDateXML("11-11-11"));
        project3.setStatus(DocumentStatus.PLANNED);
        project3.setId("b1713875-afc2-49d3-ae6f-cfb247bc6474");
        project3.setUserId("7f4ae477-2293-4026-b95a-0ce9de07c016");
    }

    void createTasks() throws java.lang.Exception {
        task1 = new Task();
        task1.setName("Тестовый таск 1");
        task1.setDescription("Тестовое описание 1");
        task1.setProjectId("5152afff-6c04-4587-8df3-36e5f40779f0");
        task1.setDateStart(DateUtil.parseDateXML("09-09-99"));
        task1.setDateFinish(DateUtil.parseDateXML("01-01-01"));
        task1.setStatus(DocumentStatus.IN_PROGRESS);
        task1.setId("5152afff-6c04-4587-8df3-36e5f40779f0");
        task1.setUserId("98ebe1bf-eb74-4db5-a835-dd7f47a1e21b");

        task2 = new Task();
        task2.setName("Тестовый таск 2");
        task2.setDescription("Тестовое описание 2");
        task2.setProjectId("c0a2f36e-95dc-4812-98af-4013e30f9e1f");
        task2.setDateStart(DateUtil.parseDateXML("08-08-88"));
        task2.setDateFinish(DateUtil.parseDateXML("12-12-12"));
        task2.setStatus(DocumentStatus.READY);
        task2.setId("a5c413e5-d9b0-4fd9-8467-04124765fb4c");
        task2.setUserId("4ea9da55-52f1-47e7-9c5e-ee44a0a95819");

        task3 = new Task();
        task3.setName("Тестовый таск 3");
        task3.setDescription("Тестовое описание 3");
        task3.setProjectId("b1713875-afc2-49d3-ae6f-cfb247bc6474");
        task3.setDateStart(DateUtil.parseDateXML("07-07-77"));
        task3.setDateFinish(DateUtil.parseDateXML("11-11-11"));
        task3.setStatus(DocumentStatus.PLANNED);
        task3.setId("1dbe132c-7aa4-4394-9d90-8b8eb1cef023");
        task3.setUserId("7f4ae477-2293-4026-b95a-0ce9de07c016");
    }
}
