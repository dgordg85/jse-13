**РАЗРАБОТКА КОНСОЛЬНОГО ПРИЛОЖЕНИЯ**
=====================

GITLAB URL
-----------------------------------

[https://gitlab.com/dgordg85/jse-13](https://gitlab.com/dgordg85/jse-13 "GITLAB")

ТРЕБОВАНИЯ К SOFTWARE
-----------------------------------
* JRE

ОПИСАНИЕ СТЕКА ТЕХНОЛОГИЙ
-----------------------------------
* JDK 1.8
* MAVEN 3.4.0
* IDEA
* MYSQL 5.5

ИМЯ РАЗРАБОТЧИКА И КОНТАКТЫ
-----------------------------------
    Александр К.
    Skype: roverc0m
    E-mail: dgordg85@gmail.com

КОМАНДЫ ДЛЯ СБОРКИ ПРИЛОЖЕНИЯ
-----------------------------------
    git clone http://gitlab.volnenko.school/Kozyrev/jse-13.git
    cd jse-13
    mvn clean install
    
КОМАНДА ДЛЯ ЗАПУСКА СЕРВЕРА
-----------------------------------
    java -jar tm-server/target/task-manager-server/bin/tm_server-2.1.jar
    
КОМАНДА ДЛЯ ЗАПУСКА КЛИЕНТА
-----------------------------------
    java -jar tm-client/target/task-manager-client/bin/tm_clients-2.1.jar
    
ДОКУМЕНТАЦИЯ
-----------------------------------
    tm-server/target/docs/apidocs/index.html
